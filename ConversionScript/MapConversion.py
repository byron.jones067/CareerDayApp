from sys import argv
import json

dir = "../CareerDayApp2.0/CareerDayApp2.0/App_Data/" + argv[1]
out_file = "../CareerDayApp2.0/CareerDayApp2.0/App_Data/new_" + argv[1]


def modifyConfig(dataframe):
    d = {}
    d["height"] = dataframe["height"]
    d["width"] = dataframe["width"]
    d["rd"] = []
    d["cellContents"] = []

    for i in range(dataframe["height"]):
        d["cellContents"].append([])
        isBooth = False
        for j in range(dataframe["width"]):
            if any(char.isdigit() for char in dataframe["cellContents"][i][j]):
                isBooth = True
            else:
                isBooth = False

            temp = {
                "text": dataframe["cellContents"][i][j],
                "cellColor": "#fafad2",
                "textColor": "#000000",
                "border": [
                    True,
                    True,
                    True,
                    True
                ],
                "isBooth": isBooth
            }
            d["cellContents"][i].append(temp)

    # print(d)
    return d


with open(dir, 'r') as json_data:
    d = json.load(json_data)

dataframe = modifyConfig(d)


with open(out_file, 'w') as outfile:
    json.dump(dataframe, outfile, indent=4)
