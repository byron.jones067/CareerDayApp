from sys import argv
import json

dir = "../CareerDayApp2.0/CareerDayApp2.0/App_Data/" + argv[1]


def modify_config(dataframe):
    # change the cellColor of any cell w/
    # a single character for its text
    height = dataframe['height']
    width = dataframe['width']
    cell_list = dataframe['cellContents']
    lightgray_hex = '#D3D3D3'
    for i in range(height):
        for j in range(width):
            if len(cell_list[i][j]['text']) == 1:
                cell_list[i][j]['cellColor'] = lightgray_hex

    return d


with open(dir, 'r') as json_data:
    d = json.load(json_data)

dataframe = modify_config(d)

with open(dir, 'w') as outfile:
    json.dump(dataframe, outfile, indent=4)
