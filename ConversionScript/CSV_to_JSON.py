import random
from random import randrange
rawData = open('Majors.csv')
activeMajor = ''
companies = []
roomPrefix = ['U', 'UA', 'L', 'LA']

class company:
  def __init__(self, name, numBooth, majors = [], industries='industry'):
    self.name = str(name)
    self.numBooth = str(numBooth)
    self.majors = []
    self.industries = industries

  def __str__(self):
    return self.name + ', ' + self.numBooth + ', ' + ', '.join(self.majors) + ', '+ self.industries + '\n'

  def addNewMajor(self, name, level):
    list = ['Freshman',
            'Sophomores',
            'Juniors and Non- Graduating Seniors',
            'Non- Graduating Masters / Professional',
            'Non- Graduating Doctorate',
            'Graduating Seniors',
            'Graduating Masters / Professional',
            'Graduating Doctorate'
          ]
    returnVal = []
    for i in range(8):
      if level[i] == 1:
        returnVal.append(list[i])
      self.majors.append(major(name, returnVal))

class major:
  def __init__(self, name, level):
    self.name = name
    self.level = level

  def __str__(self):
    return self.name + ', ' + self.level

for line in rawData:
  x = line.split(',')
  if x[1] == 'Freshman':
    activeMajor = x[0]
  else:
    levels = []
    for i in range(1,9):
      if x[i] == 'Yes' or x[i] == 'Yes\n':
        levels.append(1)
      else:
        levels.append(0)
      
    matchFound = False
    current = company('', 0)
    for i in companies:
      if x[0] == i.name:
        if i.name not in i.majors:
          matchFound = True
          current = i
          current.addNewMajor(activeMajor, levels)
          break

    if not matchFound:
      
      random_index = randrange(0, len(roomPrefix))
      prefix = roomPrefix[random_index]
      
      numBooth = random.randint(0, 100)
      currCompany = company(x[0], prefix + str(numBooth))
      currCompany.addNewMajor(activeMajor, levels)
      companies.append(currCompany)

f = open('outputTest.json','w')
f.write('[\n')
for i in range(0, len(companies)):
  f.write(
          '{\n\tname: \'' + companies[i].name + '\',' + 
          '\n\tbooth: \'' + companies[i].numBooth + '\',' + '\n\tindustry: \''  + companies[i].industries + '\',' + '\n\tmajors:[')

  for j in range(0, len(companies[i].majors)-8, 8):
    f.write('{\n'+6*'\t' + 'major: \'' + companies[i].majors[j].name +
            '\',\n' + 6*'\t' + 'levels: [')

    for k in range(0, len(companies[i].majors[j].level)-1):
      f.write('\'' + companies[i].majors[j].level[k] + '\',')
    f.write('\'' + companies[i].majors[j].level[-1] + '\'')
    #f.write('\'' + companies[i].majors[j].level[-1] + '\'')
    f.write('],\n' + 3*'\t' + '},\n')

  f.write('{\n'+6*'\t' + 'major: \'' + companies[i].majors[-1].name +
            '\',\n' + 6*'\t' + 'levels: [')
  for k in range(0, len(companies[i].majors[-1].level)-1):
    f.write('\'' + companies[i].majors[-1].level[k] + '\',')
  f.write('\'' + companies[i].majors[-1].level[-1] + '\'')
  #f.write('\'' + companies[i].majors[j].level[-1] + '\'')
  f.write('],\n' + 3*'\t' + '}\n')
  

  f.write(3*'\t')
  f.write(']},\n')
f.write(']')
f.close()
'''
list = []

for company in companies:
  for major in company.majors:
    if major.name not in list:
      list.append(major.name)

for i in list:
  print("<option value=\"" + i +"\">" + i + "</option>")
  '''