﻿/*==========================================================================================
 * File Name: Carousel.cs
 * File Description: Contains definition for the Carousel class, which includes information about 
 * each image in the Carousel. Information about the Carousel can be set in the homeConfig.json file.
 ===========================================================================================*/

namespace CareerDayApp2._0.Models
{
    public class Carousel
    {
        public string name { set; get; }
        public string image { set; get; }
        public string altText { set; get; }
        public string caption { set; get; }

        public Carousel(string name)
        {
            this.name = name;
            image = "";
            altText = "";
            caption = "";
        }
    }
}