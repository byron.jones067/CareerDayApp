﻿/*==========================================================================================
 * File Name: Map.cs
 * File Description: Contains definition for the Map class, which includes information about 
 *  a map. Information about maps can be set in the mapConfig.json file.
 ===========================================================================================*/

namespace CareerDayApp2._0.Models
{
    public class Map
    {
        public string name { get; set; }
        public string fileName { get; set; }
        public string[] prefix { get; set; }
        public string smartMap { get; set; }
        public bool isBoothMap { get; set; }

        public Map()
        {
            name = "";
            fileName = "";
            prefix = new string[] { "", "" };
            smartMap = "";
            isBoothMap = false;
        }

        public Map(string name)
        {
            this.name = name;
            fileName = "";
            prefix = new string[] { "", "" };
            smartMap = "";
            isBoothMap = false;
        }
    }
}

