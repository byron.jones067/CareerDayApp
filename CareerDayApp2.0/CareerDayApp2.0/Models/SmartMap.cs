﻿/*==========================================================================================
 * File Name: SmartMap.cs
 * File Description: Contains definition for the SmartMap class, which includes information about 
 *  a smart map. Information about smart maps can be set in the mapConfig.json file.
 ===========================================================================================*/

namespace CareerDayApp2._0.Models
{
    public class SmartMap
    {
        public int height { get; set; }
        public int width { get; set; }
        public string[] rd { get; set;} 
        public CellContents[][] cellContents { get; set; }

        public SmartMap()
        {
            height = 0;
            width = 0;
            cellContents = new CellContents[height][];
        }

        public SmartMap(int height, int width)
        {
            this.height = height;
            this.width = width;
            cellContents = new CellContents[height][];
        }
    }
}

