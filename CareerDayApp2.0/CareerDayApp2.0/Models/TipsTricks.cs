﻿/*==========================================================================================
 * File Name: TipsTricks.cs
 * File Description: Contains definition for the TipsTricks class, which includes information displayed
 * on the tips and tricks page. Information about TipsTricks can be set in the tipsConfig.json file.
 ===========================================================================================*/

using System.Collections.Generic;

namespace CareerDayApp2._0.Models
{
    public class TipsTricks
    {
       public Dictionary<string, string> allFiles { get; set; }
    }
}