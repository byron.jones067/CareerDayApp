﻿/*==========================================================================================
 * File Name: ErrorViewModel.cs
 * File Description: Contains definition for the ErrorViewModel class, which contains 
 *  information about an error.
 ===========================================================================================*/

namespace CareerDayApp2._0.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
