﻿
namespace CareerDayApp2._0.Models
{
    public class CellContents
    {
        public bool[] border { get; set; }
        public string cellColor { get; set; }
        public string textColor { get; set; }
        public string text { get; set; }
        public bool isBooth { get; set; }

        public CellContents()
        {
            text = "H";
            cellColor = "#D3D3D3";
            textColor = "#000000";
            border = new bool[4] {
                true,
                true,
                true,
                true
            };
            isBooth = false;
        }
        public CellContents(string text)
        {
            this.text = text;
            if (text.Length == 1)
            {
                cellColor = "#FAFAD2";
            }
            else
            {
                cellColor = "#D3D3D3";
            }
            textColor = "#000000";
            border = new bool[4] {
                true,
                true,
                true,
                true
            };
            isBooth = false;
        }

        public void CreateUpper(string text)
        {
            this.text = text;
            cellColor = "#FAFAD2";
            textColor = "#000000";
            border = new bool[4] {
                true,
                true,
                false,
                true
            };
            isBooth = true;
        }

        public void CreateLower()
        {
            this.text = "";
            cellColor = "#FAFAD2";
            textColor = "#000000";
            border = new bool[4] {
                false,
                true,
                true,
                true
            };
        }
    }
}