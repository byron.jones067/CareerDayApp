﻿/*==========================================================================================
 * File Name: Home.cs
 * File Description: Contains a definition for the Home class, which contains all customizable
 *  information on the home page (obtained from homeConfig.json).
 ===========================================================================================*/

using System.Collections.Generic;

namespace CareerDayApp2._0.Models
{
    public class Home
    {
        public string semester { get; set; }
        public string date { get; set; }
        public string timeNetwork { get; set; }
        public string timeCampus { get; set; }
        public bool showDataWipe { get; set; }
        public string wipeDate { get; set; }
        public IList<Carousel> carousel { get; set; } // array of carousel objects
        public string introParagraph { get; set; }
        public string Events { get; set; }
        public string Attendance { get; set; }
        public string Commitment { get; set; }
        public string Body { get; set; }

        public Home()
        {
            semester = "";
            date = "";
            timeNetwork = "";
            timeCampus = "";
            showDataWipe = true;
            wipeDate = "";
            carousel = new List<Carousel>();
            introParagraph = "";
            Events = "";
            Attendance = "";
            Commitment = "";
            Body = "";
        }

        // Adds a new carousel object to the list of carousel
        public void AddImage()
        {
            string name = "image" + carousel.Count.ToString();
            carousel.Add(new Carousel(name));
        }

        // Remove carousel object from the list of carousel
        public void RemoveImage(int index)
        {
            try
            {
                carousel.RemoveAt(index);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                throw new System.ArgumentOutOfRangeException("index parameter is out of range", ex);
            }
        }

        // returns the carousel object
        public IList<Carousel> GetCarousel()
        {
            return carousel;
        }

        // returns size of the carousel list
        public int GetCarouselSize()
        {
            return carousel.Count;
        }
    }
}
