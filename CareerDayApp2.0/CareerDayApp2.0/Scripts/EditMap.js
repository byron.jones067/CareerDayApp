﻿
window.onload = onStart;

function onStart() {
	var c = document.getElementById("smartEditor");
	c.height = height * 60 + 2;
	c.width = width * 120 + 2;
    var ctx = c.getContext("2d");
    var canvasHeight = c.height;
    var canvasWidth = c.width;
    var cellWidth = 120;
    var cellHeight = 60;

    var y = 0;
    while (y < height) {
        var x = 0;
		while (x < width) {
			//fill rectangle first
			ctx.fillStyle = smartMap.cellContents[y][x].cellColor;
			ctx.fillRect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
			//add borders to cell
			if (smartMap.cellContents[y][x].border[0]) {
				drawBorder((x * cellWidth), (y * cellHeight), (x * cellWidth) + cellWidth, (y * cellHeight));
			}
			if (smartMap.cellContents[y][x].border[1]) {
				drawBorder((x * cellWidth) + cellWidth, (y * cellHeight), (x * cellWidth) + cellWidth, (y * cellHeight) + cellHeight);
			}
			if (smartMap.cellContents[y][x].border[2]) {
				drawBorder((x * cellWidth), (y * cellHeight) + cellHeight, (x * cellWidth) + cellWidth, (y * cellHeight) + cellHeight);
			}
			if (smartMap.cellContents[y][x].border[3]) {
				drawBorder((x * cellWidth), (y * cellHeight), (x * cellWidth), (y * cellHeight) + cellHeight);
			}
            // Create if statement to prevent printing if no cell contents
			if (smartMap.cellContents[y][x].text !== "") {
				drawText(x, y + 1, cellWidth, cellHeight, smartMap.cellContents[y][x].text, smartMap.cellContents[y][x].textColor);
			}
			else {
				if (y > 0 && smartMap.cellContents[y-1][x].isBooth && (smartMap.cellContents[y-1][x].text in xref)) {
					drawText(x, y + 1, cellWidth, cellHeight, xref[smartMap.cellContents[y-1][x].text].orgName, smartMap.cellContents[y][x].textColor);
				}
			}
            x = x + 1;
        }
        y = y + 1;
    }

    drawBorder(0, canvasHeight - 2, canvasWidth, canvasHeight - 2); 
    drawBorder(canvasWidth - 2, 0, canvasWidth - 2, canvasHeight);

	////add borders for cells
	//ctx.fillStyle = "black";
	//for (var i = 0; i < canvasWidth; i++) {
	//	ctx.beginPath();
	//	ctx.moveTo(cellWidth * i + 1, 0);
	//	ctx.lineTo(cellWidth * i + 1, canvasHeight);
	//	ctx.stroke();
	//}

	//for (var i = 0; i < canvasHeight; i++) {
	//	ctx.beginPath();
	//	ctx.moveTo(0, cellHeight * i + 1);
	//	ctx.lineTo(canvasWidth, cellHeight * i + 1);
	//	ctx.stroke();
	//}

	c.addEventListener('mousedown', colorListener, false);
}

function change(type) {
	var c = document.getElementById("smartEditor");
	var cellClick = document.getElementById("cellClick");
	var textClick = document.getElementById("textClick");
	var mapClick = document.getElementById("mapClick");
	//cell selections
	var cellColor = document.getElementById("cellLabel");
	var textColor = document.getElementById("textLabel");
	var bt = document.getElementById("label-top");
	var bl = document.getElementById("label-left");
	var bb = document.getElementById("label-bottom");
	var br = document.getElementById("label-right");
	//text selections
	var textColor = document.getElementById("textLabel");
	var boothLabel = document.getElementById("label-booth");
	//map selections
	var addRow = document.getElementById("addRow");
	var addCol = document.getElementById("addCol");
	var insertRowLabel = document.getElementById("insertRowLabel");
	var deleteRowLabel = document.getElementById("deleteRowLabel");
	var insertColLabel = document.getElementById("insertColLabel");
	var deleteColLabel = document.getElementById("deleteColLabel");
	//change
	if (type == 'cell') {
		//change listener
		c.removeEventListener('mousedown', textListener, false);
		c.removeEventListener('mousedown', mapListener, false);
		c.addEventListener('mousedown', colorListener, false);
		//changed checked
		cellClick.checked = true;
		textClick.checked = false;
		mapClick.checked = false;
		//change selections visibility
		cellColor.hidden = false;
		bl.hidden = false;
		br.hidden = false;
		bt.hidden = false;
		bb.hidden = false;
		textColor.hidden = true;
		boothLabel.hidden = true;
		addRow.hidden = true;
		addCol.hidden = true;
		insertRowLabel.hidden = true;
		deleteRowLabel.hidden = true;
		insertColLabel.hidden = true;
		deleteColLabel.hidden = true;
	}
	else if (type == 'text') {
		//change listener
		c.removeEventListener('mousedown', colorListener, false);
		c.removeEventListener('mousedown', mapListener, false);
		c.addEventListener('mousedown', textListener, false);
		//changed checked
		cellClick.checked = false;
		textClick.checked = true;
		mapClick.checked = false;
		//change selections visibility
		cellColor.hidden = true;
		bl.hidden = true;
		br.hidden = true;
		bt.hidden = true;
		bb.hidden = true;
		textColor.hidden = false;
		boothLabel.hidden = false;
		addRow.hidden = true;
		addCol.hidden = true;
		insertRowLabel.hidden = true;
		deleteRowLabel.hidden = true;
		insertColLabel.hidden = true;
		deleteColLabel.hidden = true;
	}
	else if (type == 'map') {
		//change listener
		c.removeEventListener('mousedown', textListener, false);
		c.removeEventListener('mousedown', colorListener, false);
		c.addEventListener('mousedown', mapListener, false);
		//changed checked
		cellClick.checked = false;
		textClick.checked = false;
		mapClick.checked = true;
		//change selections visibility
		cellColor.hidden = true;
		bl.hidden = true;
		br.hidden = true;
		bt.hidden = true;
		bb.hidden = true;
		textColor.hidden = true;
		boothLabel.hidden = true;
		addRow.hidden = false;
		addCol.hidden = false;
		insertRowLabel.hidden = false;
		deleteRowLabel.hidden = false;
		insertColLabel.hidden = false;
		deleteColLabel.hidden = false;
	}
}

function changeFormat(type) {
	var insertRow = document.getElementById("insertRow");
	var deleteRow = document.getElementById("deleteRow");
	var insertCol = document.getElementById("insertCol");
	var deleteCol = document.getElementById("deleteCol");
	if (type == 'ir') {
		insertRow.checked = true;
		insertCol.checked = false;
		deleteCol.checked = false;
		deleteRow.checked = false;
	}
	else if (type == 'ic') {
		insertRow.checked = false;
		insertCol.checked = true;
		deleteCol.checked = false;
		deleteRow.checked = false;
	}
	else if (type == 'dc') {
		insertRow.checked = false;
		insertCol.checked = false;
		deleteCol.checked = true;
		deleteRow.checked = false;
	}
	else if (type == 'dr') {
		insertRow.checked = false;
		insertCol.checked = false;
		deleteCol.checked = false;
		deleteRow.checked = true;
	}
}

var textListener = function text(event) {
	var x = event.pageX;
	var y = event.pageY;
	var c = document.getElementById("smartEditor");
	var ctx = c.getContext("2d");
	var canvasHeight = c.height;
	var canvasWidth = c.width;
	var cellWidth = 120;
	var cellHeight = 60;
	x -= c.offsetLeft;
	y -= c.offsetTop;
	var navbarHeight = 90; // Height of navbar to fix y offset on listener  
	y -= navbarHeight;
	x = x - x % cellWidth;
	y = y - y % cellHeight;

	ctx.fillStyle = document.getElementById("cellColor" + (y / cellHeight) + ":" + (x / cellWidth)).value;
	ctx.fillRect(x, y, cellWidth, cellHeight);
	if (document.getElementById("border-top").checked) {
		drawBorder(x, y, x + cellWidth, y);
	}
	if (document.getElementById("border-right").checked) {
		drawBorder(x + cellWidth, y, x + cellWidth, y + cellHeight);
	}
	if (document.getElementById("border-bottom").checked) {
		drawBorder(x, y + cellHeight, x + cellWidth, y + cellHeight);
	}
	if (document.getElementById("border-left").checked) {
		drawBorder(x, y, x, y + cellHeight);
	}
	//prompt for text for cell
	var input = prompt("Type text for cell", document.getElementById("text" + ((y / cellHeight)) + ":" + (x / cellWidth)).value);
	if (input == null) {
		drawText(x / cellWidth, (y / cellHeight) + 1, cellWidth, cellHeight, "");
		document.getElementById("text" + (y / cellHeight) + ":" + (x / cellWidth)).value = "";
	}
	else {
		drawText(x / cellWidth, (y / cellHeight) + 1, cellWidth, cellHeight, input, document.getElementById("textColor").value);
		document.getElementById("text" + (y / cellHeight) + ":" + (x / cellWidth)).value = input;
	}
	document.getElementById("textColor" + (y / cellHeight) + ":" + (x / cellWidth)).value = document.getElementById("textColor").value;
	//changes current selected cell and bottom cell, unless at bottom already, to booth cell
	if (document.getElementById("isBooth").checked) {
		document.getElementById("isBooth" + (y / cellHeight) + ":" + (x / cellWidth)).value = true;
		//update bottom booth cell
		if ((y / cellHeight) < (height - 1)) {
			document.getElementById("text" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value = "";
			document.getElementById("cellColor" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value = document.getElementById("cellColor" + (y / cellHeight) + ":" + (x / cellWidth)).value;
			document.getElementById("textColor" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value = document.getElementById("textColor" + (y / cellHeight) + ":" + (x / cellWidth)).value;
			ctx.fillStyle = document.getElementById("cellColor" + (y / cellHeight) + ":" + (x / cellWidth)).value;
			ctx.fillRect(x, y + cellHeight, cellWidth, cellHeight);
			if (document.getElementById("text" + (y / cellHeight) + ":" + (x / cellWidth)).value in xref) {
				drawText(x / cellWidth, (y / cellHeight) + 2, cellWidth, cellHeight, xref[document.getElementById("text" + (y / cellHeight) + ":" + (x / cellWidth)).value].orgName, document.getElementById("textColor" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value);
			}
			//change borders for both cells with upper first, then lower
			document.getElementById("topBorder" + (y / cellHeight) + ":" + (x / cellWidth)).value = true;
			document.getElementById("leftBorder" + (y / cellHeight) + ":" + (x / cellWidth)).value = true;
			document.getElementById("bottomBorder" + (y / cellHeight) + ":" + (x / cellWidth)).value = false;
			document.getElementById("rightBorder" + (y / cellHeight) + ":" + (x / cellWidth)).value = true;
			drawBorder(x, y, x + cellWidth, y);
			drawBorder(x + cellWidth, y, x + cellWidth, y + cellHeight);
			drawBorder(x, y, x, y + cellHeight);
			document.getElementById("topBorder" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value = false;
			document.getElementById("leftBorder" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value = true;
			document.getElementById("bottomBorder" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value = true;
			document.getElementById("rightBorder" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value = true;
			drawBorder(x, y + cellHeight + cellHeight, x + cellWidth, y + cellHeight + cellHeight);
			drawBorder(x + cellWidth, y + cellHeight, x + cellWidth, y + cellHeight + cellHeight);
			drawBorder(x, y + cellHeight, x, y + cellHeight + cellHeight);
			
		}
	} else {
		document.getElementById("isBooth" + (y / cellHeight) + ":" + (x / cellWidth)).value = false;
		//ctx.fillStyle = document.getElementById("cellColor" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value;
		//ctx.fillRect(x, y + cellHeight, cellWidth, cellHeight);
		//if (document.getElementById("topBorder" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value) {
		//	drawBorder(x, y + cellHeight, x + cellWidth, y + cellHeight);
		//}
		//if (document.getElementById("rightBorder" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value) {
		//	drawBorder(x + cellWidth, y + cellHeight, x + cellWidth, y + cellHeight + cellHeight);
		//}
		//if (document.getElementById("bottomBorder" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value) {
		//	drawBorder(x, y + cellHeight + cellHeight, x + cellWidth, y + cellHeight + cellHeight);
		//}
		//if (document.getElementById("leftBorder" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value) {
		//	drawBorder(x, y + cellHeight, x, y + cellHeight + cellHeight);
		//}
		//drawText(x / cellWidth, (y / cellHeight) + 2, cellWidth, cellHeight, document.getElementById("text" + ((y / cellHeight) + 1) + ":" + (x / cellWidth)).value);
	}
}

var colorListener = function color(event) {
    var x = event.pageX;
    var y = event.pageY;
    var c = document.getElementById("smartEditor");
    var ctx = c.getContext("2d");
    var canvasHeight = c.height;
    var canvasWidth = c.width;
    var cellWidth = 120;
    var cellHeight = 60;
    x -= c.offsetLeft;
    y -= c.offsetTop;
    var navbarHeight = 90; // Height of navbar to fix y offset on listener  
    y -= navbarHeight;
    x = x - x % cellWidth;
	y = y - y % cellHeight;
    ctx.fillStyle = document.getElementById("color").value;
	ctx.fillRect(x, y, cellWidth, cellHeight);
	//initialize borders array
	var borders = [false, false, false, false];
	if (document.getElementById("border-top").checked) {
		borders[0] = true;
        drawBorder(x, y, x + cellWidth, y);
    }
	if (document.getElementById("border-right").checked) {
		borders[1] = true;
        drawBorder(x + cellWidth, y, x + cellWidth, y + cellHeight);
    }
	if (document.getElementById("border-bottom").checked) {
		borders[2] = true;
        drawBorder(x, y + cellHeight, x + cellWidth, y + cellHeight);
    }
	if (document.getElementById("border-left").checked) {
		borders[3] = true;
        drawBorder(x, y, x, y + cellHeight);
	}
	drawText(x / cellWidth, (y / cellHeight) + 1, cellWidth, cellHeight, document.getElementById("text" + (y / cellHeight) + ":" + (x / cellWidth)).value, document.getElementById("textColor" + (y / cellHeight) + ":" + (x / cellWidth)).value);
	if (document.getElementById("text" + (y / cellHeight) + ":" + (x / cellWidth)).value == "" && (y / cellHeight) - 1 >= 0 && document.getElementById("isBooth" + ((y / cellHeight) - 1) + ":" + (x / cellWidth)).value && (document.getElementById("text" + ((y / cellHeight) - 1) + ":" + (x / cellWidth)).value in xref)) {
		drawText(x / cellWidth, (y / cellHeight) + 1, cellWidth, cellHeight, xref[document.getElementById("text" + ((y / cellHeight) - 1) + ":" + (x / cellWidth)).value].orgName, document.getElementById("textColor" + (y / cellHeight) + ":" + (x / cellWidth)).value);
	}
    y = y / cellHeight;
    x = x / cellWidth;
	//change values on values table
	document.getElementById("topBorder" + y + ":" + x).value = borders[0];
	document.getElementById("rightBorder" + y + ":" + x).value = borders[1];
	document.getElementById("bottomBorder" + y + ":" + x).value = borders[2];
	document.getElementById("leftBorder" + y + ":" + x).value = borders[3];
	document.getElementById("cellColor" + y + ":" + x).value = document.getElementById("color").value;
}

var mapListener = function mapFormat(event) {
	var x = event.pageX;
	var y = event.pageY;
	var c = document.getElementById("smartEditor");
	var ctx = c.getContext("2d");
	var canvasHeight = c.height;
	var canvasWidth = c.width;
	var cellWidth = 120;
	var cellHeight = 60;
	x -= c.offsetLeft;
	y -= c.offsetTop;
	var navbarHeight = 90; // Height of navbar to fix y offset on listener  
	y -= navbarHeight;
	x = x - x % cellWidth;
	y = y - y % cellHeight;
	y = y / cellHeight;
	x = x / cellWidth;
	var insertRow = document.getElementById("insertRow");
	var deleteRow = document.getElementById("deleteRow");
	var insertCol = document.getElementById("insertCol");
	var deleteCol = document.getElementById("deleteCol");
	var table = document.getElementById("values");
	var rowLength = table.rows[0].cells.length;

	if (insertRow.checked) {
		document.getElementById("mapHeight").value = parseInt(document.getElementById("mapHeight").value) + 1;
		//shift all lower rows down by one
		if (table.rows.length == 1) {
			for (var j = 0; j < rowLength; j++) {
				shiftCells(0, 1, j, 0);
			}
		}
		else if (table.rows.length == 2) {
			for (var j = 0; j < rowLength; j++) {
				shiftCells(0, 1, j, 1);
			}
			for (var j = 0; j < rowLength; j++) {
				shiftCells(0, 1, j, 0);
			}
		}
		else {
			for (var i = table.rows.length - 1; i >= y; i--) {
				for (var j = 0; j < rowLength; j++) {
					shiftCells(0, 1, j, i);
				}
			}
		}
		var newRow = table.insertRow(y);
		for (var i = 0; i < rowLength; i++) {
			var newCell = newRow.insertCell(i);
			var html = '<input id="topBorder' + y + ':' + i + '" name="topBorder' + y + ':' + i + '" value=true />';
			html += '<input id="rightBorder' + y + ':' + i + '" name="rightBorder' + y + ':' + i + '" value=true />';
			html += '<input id="bottomBorder' + y + ':' + i + '" name="bottomBorder' + y + ':' + i + '" value=true />';
			html += '<input id="leftBorder' + y + ':' + i + '" name="leftBorder' + y + ':' + i + '" value=true />';
			html += '<input id="cellColor' + y + ':' + i + '" name="cellColor' + y + ':' + i + '" value="#d3d3d3" />';
			html += '<input id="textColor' + y + ':' + i + '" name="textColor' + y + ':' + i + '" value="#000000" />';
			html += '<input id="text' + y + ':' + i + '" name="text' + y + ':' + i + '" value="" />';
			html += '<input id="isBooth' + y + ':' + i + '" name="isBooth' + y + ':' + i + '" value=false />';
			newCell.innerHTML = html;
		}
	}
	else if (insertCol.checked) {
		document.getElementById("mapWidth").value = parseInt(document.getElementById("mapWidth").value) + 1;
		//shift all right cols right by one
		for (var i = 0; i < table.rows.length; i++) {
			for (var j = rowLength - 1; j >= x; j--) {
				//alert(j + " " + i);
				shiftCells(1, 0, j, i);
			}
		}
		for (var i = 0; i < table.rows.length; i++) {
			var row = table.rows[i];
			var newCell = row.insertCell(x);
			var html = '<input id="topBorder' + i + ':' + x + '" name="topBorder' + i + ':' + x + '" value=true />';
			html += '<input id="rightBorder' + i + ':' + x + '" name="rightBorder' + i + ':' + x + '" value=true />';
			html += '<input id="bottomBorder' + i + ':' + x + '" name="bottomBorder' + i + ':' + x + '" value=true />';
			html += '<input id="leftBorder' + i + ':' + x + '" name="leftBorder' + i + ':' + x + '" value=true />';
			html += '<input id="cellColor' + i + ':' + x + '" name="cellColor' + i + ':' + x + '" value="#d3d3d3" />';
			html += '<input id="textColor' + i + ':' + x + '" name="textColor' + i + ':' + x + '" value="#000000" />';
			html += '<input id="text' + i + ':' + x + '" name="text' + i + ':' + x + '" value="" />';
			html += '<input id="isBooth' + i + ':' + x + '" name="isBooth' + i + ':' + x + '" value=false />';
			newCell.innerHTML = html;
		}
	}
	else if (deleteRow.checked) {
		document.getElementById("mapHeight").value = parseInt(document.getElementById("mapHeight").value) - 1;
		table.deleteRow(y);
		//shift all forward rows down by one
		for (var i = y+1; i <= table.rows.length; i++) {
			for (var j = 0; j < rowLength; j++) {
				shiftCells(0, -1, j, i);
			}
		}
		if (table.rows.length == 0) {
			document.getElementById("mapWidth").value = 0;
		}
	}
	else if (deleteCol.checked) {
		document.getElementById("mapWidth").value = parseInt(document.getElementById("mapWidth").value) - 1;
		for (var i = 0; i < table.rows.length; i++) {
			var row = table.rows[i];
			row.deleteCell(x);
		}
		//shift all right cols left by one
		for (var i = 0; i < table.rows.length; i++) {
			for (var j = x + 1; j < rowLength; j++) {
				shiftCells(-1, 0, j, i);
			}
		}
		if (table.rows[0].cells.length == 0) {
			document.getElementById("mapHeight").value = 0;
		}
	}
	redraw(table);
}

function addNew(type) {
	var table = document.getElementById("values");
	if (type == 'row') {
		document.getElementById("mapHeight").value = parseInt(document.getElementById("mapHeight").value) + 1;
		if (table.rows.length == 0 || table.rows[0].cells.length == 0) {
			table.innerHTML = "";
			document.getElementById("mapWidth").value = 1;
			var newRow = table.insertRow(0);
			var newCell = newRow.insertCell(0);
			var html = '<input id="topBorder' + 0 + ':' + 0 + '" name="topBorder' + 0 + ':' + 0 + '" value="True" />';
			html += '<input id="rightBorder' + 0 + ':' + 0 + '" name="rightBorder' + 0 + ':' + 0 + '" value="True" />';
			html += '<input id="bottomBorder' + 0 + ':' + 0 + '" name="bottomBorder' + 0 + ':' + 0 + '" value="True" />';
			html += '<input id="leftBorder' + 0 + ':' + 0 + '" name="leftBorder' + 0 + ':' + 0 + '" value="True" />';
			html += '<input id="cellColor' + 0 + ':' + 0 + '" name="cellColor' + 0 + ':' + 0 + '" value="#d3d3d3" />';
			html += '<input id="textColor' + 0 + ':' + 0 + '" name="textColor' + 0 + ':' + 0 + '" value="#000000" />';
			html += '<input id="text' + 0 + ':' + 0 + '" name="text' + 0 + ':' + 0 + '" value="" />';
			html += '<input id="isBooth' + 0 + ':' + 0 + '" name="isBooth' + 0 + ':' + 0 + '" value="False" />';
			newCell.innerHTML = html;
		}
		else {
			var newRow = table.insertRow(table.rows.length);
			for (var i = 0; i < table.rows[0].cells.length; i++) {
				var newCell = newRow.insertCell(i);
				var html = '<input id="topBorder' + (table.rows.length - 1) + ':' + i + '" name="topBorder' + (table.rows.length - 1) + ':' + i + '" value="True" />';
				html += '<input id="rightBorder' + (table.rows.length - 1) + ':' + i + '" name="rightBorder' + (table.rows.length - 1) + ':' + i + '" value="True" />';
				html += '<input id="bottomBorder' + (table.rows.length - 1) + ':' + i + '" name="bottomBorder' + (table.rows.length - 1) + ':' + i + '" value="True" />';
				html += '<input id="leftBorder' + (table.rows.length - 1) + ':' + i + '" name="leftBorder' + (table.rows.length - 1) + ':' + i + '" value="True" />';
				html += '<input id="cellColor' + (table.rows.length - 1) + ':' + i + '" name="cellColor' + (table.rows.length - 1) + ':' + i + '" value="#d3d3d3" />';
				html += '<input id="textColor' + (table.rows.length - 1) + ':' + i + '" name="textColor' + (table.rows.length - 1) + ':' + i + '" value="#000000" />';
				html += '<input id="text' + (table.rows.length - 1) + ':' + i + '" name="text' + (table.rows.length - 1) + ':' + i + '" value="" />';
				html += '<input id="isBooth' + (table.rows.length - 1) + ':' + i + '" name="isBooth' + (table.rows.length - 1) + ':' + i + '" value="False" />';
				newCell.innerHTML = html;
				//newCell.appendChild(createInput(table.rows.length, i, "topBorder", true));
				//newCell.appendChild(createInput(table.rows.length, i, "rightBorder", true));
				//newCell.appendChild(createInput(table.rows.length, i, "bottomBorder", true));
				//newCell.appendChild(createInput(table.rows.length, i, "leftBorder", true));
				//newCell.appendChild(createInput(table.rows.length, i, "cellColor", "#d3d3d3"));
				//newCell.appendChild(createInput(table.rows.length, i, "textColor", "#000000"));
				//newCell.appendChild(createInput(table.rows.length, i, "text", ""));
				//newCell.appendChild(createInput(table.rows.length, i, "isBooth", false));
			}
		}
	}
	else if (type == 'col') {
		document.getElementById("mapWidth").value = parseInt(document.getElementById("mapWidth").value) + 1;
		if (table.rows.length == 0 || table.rows[0].cells.length == 0) {
			table.innerHTML = "";
			document.getElementById("mapHeight").value = 1;
			var newRow = table.insertRow(0);
			var newCell = newRow.insertCell(0);
			var html = '<input id="topBorder' + 0 + ':' + 0 + '" name="topBorder' + 0 + ':' + 0 + '" value="True" />';
			html += '<input id="rightBorder' + 0 + ':' + 0 + '" name="rightBorder' + 0 + ':' + 0 + '" value="True" />';
			html += '<input id="bottomBorder' + 0 + ':' + 0 + '" name="bottomBorder' + 0 + ':' + 0 + '" value="True" />';
			html += '<input id="leftBorder' + 0 + ':' + 0 + '" name="leftBorder' + 0 + ':' + 0 + '" value="True" />';
			html += '<input id="cellColor' + 0 + ':' + 0 + '" name="cellColor' + 0 + ':' + 0 + '" value="#d3d3d3" />';
			html += '<input id="textColor' + 0 + ':' + 0 + '" name="textColor' + 0 + ':' + 0 + '" value="#000000" />';
			html += '<input id="text' + 0 + ':' + 0 + '" name="text' + 0 + ':' + 0 + '" value="" />';
			html += '<input id="isBooth' + 0 + ':' + 0 + '" name="isBooth' + 0 + ':' + 0 + '" value="False" />';
			newCell.innerHTML = html;
		}
		else {
			var rowLength = table.rows[0].cells.length;
			for (var i = 0; i < table.rows.length; i++) {
				var row = table.rows[i];
				var newCell = row.insertCell(rowLength);
				var html = '<input id="topBorder' + i + ':' + rowLength + '" name="topBorder' + i + ':' + rowLength + '" value=true />';
				html += '<input id="rightBorder' + i + ':' + rowLength + '" name="rightBorder' + i + ':' + rowLength + '" value=true />';
				html += '<input id="bottomBorder' + i + ':' + rowLength + '" name="bottomBorder' + i + ':' + rowLength + '" value=true />';
				html += '<input id="leftBorder' + i + ':' + rowLength + '" name="leftBorder' + i + ':' + rowLength + '" value=true />';
				html += '<input id="cellColor' + i + ':' + rowLength + '" name="cellColor' + i + ':' + rowLength + '" value="#d3d3d3" />';
				html += '<input id="textColor' + i + ':' + rowLength + '" name="textColor' + i + ':' + rowLength + '" value="#000000" />';
				html += '<input id="text' + i + ':' + rowLength + '" name="text' + i + ':' + rowLength + '" value="" />';
				html += '<input id="isBooth' + i + ':' + rowLength + '" name="isBooth' + i + ':' + rowLength + '" value=false />';
				newCell.innerHTML = html;
			}
		}
	}
	redraw(table);
}

function drawBorder(x, y, dx, dy) {
    var c = document.getElementById("smartEditor");
	var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.moveTo(x + 1, y + 1);
    ctx.lineTo(dx + 1, dy + 1);
    ctx.stroke();
}

function drawText(x, y, cellWidth, cellHeight, str, fillColor='#000000') {
    var c = document.getElementById("smartEditor");
	var ctx = c.getContext("2d");
	x = cellWidth * (x + 0.5);
	y = cellHeight * (y - 0.5);
	ctx.font = "11px Comic Sans MS";
	ctx.fillStyle = fillColor;
	ctx.textAlign = "center";
	//handle text wrap
	var words = str.split(" ");
	var line = "";
	var lines = [];
	for (var n = 0; n < words.length; n++) {
		var testLine = line + words[n] + " ";
		var metrics = ctx.measureText(testLine);
		var testWidth = metrics.width;
		if (testWidth > (cellWidth * .85) && n > 0) {
			lines.push(line);
			line = words[n] + " ";
		} else {
			line = testLine;
		}
	}
	lines.push(line);
	var lineHeight = cellHeight * .2;
	if (lines.length > 2) {
		y -= (cellHeight * .1);
		ctx.font = "9px Comic Sans MS";
	}
	if (lines.length > 3) {
		y -= (cellHeight * .1);
	}
	if (lines.length > 4) {
		y -= (cellHeight * .1);
		ctx.font = "8px Comic Sans MS";
	}
	for (var l = 0; l < lines.length; l++) {
		ctx.fillText(lines[l], x, y);
		y += lineHeight;
	}
}

function downloadImage() {
	var c = document.getElementById("smartEditor");
	var link = document.createElement('a');
	var name = mapName.replace(" ", "") + ".png"
	link.download = name.valueOf();
	link.href = c.toDataURL()
	document.body.appendChild(link);
	link.click();
}

function redraw(table) {
	//var table = document.getElementById("values");
	var c = document.getElementById("smartEditor");
	c.height = table.rows.length * 60 + 2;
	c.width = table.rows[0].cells.length * 120 + 2;
	var ctx = c.getContext("2d");
	var canvasHeight = c.height;
	var canvasWidth = c.width;
	var cellWidth = 120;
	var cellHeight = 60;

	//clear canvas first
	//ctx.save();
	//ctx.setTransform(1, 0, 0, 1, 0, 0);
	//ctx.clearRect(0, 0, c.width, c.height);
	//ctx.restore();
	//draw canvas based on table
	for (var y = 0; y < table.rows.length; y++) {
		for (var x = 0; x < table.rows[y].cells.length; x++) {
			//fill rectangle first
			ctx.fillStyle = document.getElementById("cellColor" + y + ":" + x).value;
			ctx.fillRect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
			//add borders to cell
			if (document.getElementById("topBorder" + y + ":" + x).value) {
				drawBorder((x * cellWidth), (y * cellHeight), (x * cellWidth) + cellWidth, (y * cellHeight));
			}
			if (document.getElementById("rightBorder" + y + ":" + x).value) {
				drawBorder((x * cellWidth) + cellWidth, (y * cellHeight), (x * cellWidth) + cellWidth, (y * cellHeight) + cellHeight);
			}
			if (document.getElementById("bottomBorder" + y + ":" + x).value) {
				drawBorder((x * cellWidth), (y * cellHeight) + cellHeight, (x * cellWidth) + cellWidth, (y * cellHeight) + cellHeight);
			}
			if (document.getElementById("leftBorder" + y + ":" + x).value) {
				drawBorder((x * cellWidth), (y * cellHeight), (x * cellWidth), (y * cellHeight) + cellHeight);
			}
			// Create if statement to prevent printing if no cell contents
			if (document.getElementById("text" + y + ":" + x).value !== "") {
				drawText(x, y + 1, cellWidth, cellHeight, document.getElementById("text" + y + ":" + x).value, document.getElementById("textColor" + y + ":" + x).value);
			}
			else {
				//if (true) {
				//	var test = "isBooth" + (y) + ":" + x;
				//	alert("reachd " + x + " " + /*document.getElementById("isBooth" + (y) + ":" + x).value*/ test);
				//}
				if (y > 0 && document.getElementById("isBooth" + (y - 1) + ":" + x).value && (document.getElementById("text" + (y - 1) + ":" + x).value in xref)) {
					drawText(x, y + 1, cellWidth, cellHeight, xref[document.getElementById("text" + (y-1) + ":" + x).value].orgName, document.getElementById("textColor" + y + ":" + x).value);
				}
			}
		}
	}
}

function createInput(y, x, label, value) {
	var e = document.createElement("input");
	e.id = label + y + ":" + x;
	e.name = label + y + ":" + x;
	e.value = value;
	return e;
}

function shiftCells(shiftJBy, shiftIBy, j, i) {
	var tbl = document.getElementById("topBorder" + i + ":" + j);
	var string = "topBorder" + (i + shiftIBy) + ":" + (j + shiftJBy);
	tbl.id = "topBorder" + (i + shiftIBy) + ":" + (j + shiftJBy);
	tbl.name = "topBorder" + (i + shiftIBy) + ":" + (j + shiftJBy);
	var tbl = document.getElementById("leftBorder" + i + ":" + j);
	tbl.id = "leftBorder" + (i + shiftIBy) + ":" + (j + shiftJBy);
	tbl.name = "leftBorder" + (i + shiftIBy) + ":" + (j + shiftJBy);
	var tbl = document.getElementById("bottomBorder" + i + ":" + j);
	tbl.id = "bottomBorder" + (i + shiftIBy) + ":" + (j + shiftJBy);
	tbl.name = "bottomBorder" + (i + shiftIBy) + ":" + (j + shiftJBy);
	var tbl = document.getElementById("rightBorder" + i + ":" + j);
	tbl.id = "rightBorder" + (i + shiftIBy) + ":" + (j + shiftJBy);
	tbl.name = "rightBorder" + (i + shiftIBy) + ":" + (j + shiftJBy);
	var tbl = document.getElementById("cellColor" + i + ":" + j);
	tbl.id = "cellColor" + (i + shiftIBy) + ":" + (j + shiftJBy);
	tbl.name = "cellColor" + (i + shiftIBy) + ":" + (j + shiftJBy);
	var tbl = document.getElementById("textColor" + i + ":" + j);
	tbl.id = "textColor" + (i + shiftIBy) + ":" + (j + shiftJBy);
	tbl.name = "textColor" + (i + shiftIBy) + ":" + (j + shiftJBy);
	var tbl = document.getElementById("text" + i + ":" + j);
	tbl.id = "text" + (i + shiftIBy) + ":" + (j + shiftJBy);
	tbl.name = "text" + (i + shiftIBy) + ":" + (j + shiftJBy);
	var tbl = document.getElementById("isBooth" + i + ":" + j);
	tbl.id = "isBooth" + (i + shiftIBy) + ":" + (j + shiftJBy);
	tbl.name = "isBooth" + (i + shiftIBy) + ":" + (j + shiftJBy);
}
//function objectToJSON(smartMap) {
//    var jsonString = JSON.stringify(smartMap);
//    $.ajax({
//        url: '@Url.Action("EditMap")',
//        type: "POST",
//        data: jsonString,
//        success: alert(smartMap.cellContents[0][0].cellColor),
//    });
//}