﻿
function downloadImage() {
	var c = document.getElementById("smartMap");
	c.height = height * 60 /*3000*/ + 2;
	c.width = width * 120 /*2400*/ + 2;
	var ctx = c.getContext("2d");
	var canvasHeight = c.height;
	var canvasWidth = c.width;
	var cellWidth = /*2400/width*/120;
	var cellHeight = /*3000/height*/60;
	var y = 0;
	while (y < height) {
		var x = 0;
		while (x < width) {
            //fill rectangle first
            var currCell = smartMap.cellContents[y][x]

			ctx.fillStyle = currCell.cellColor;
			ctx.fillRect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
			//add borders to cell
			if (currCell.border[0]) {
				drawBorder((x * cellWidth), (y * cellHeight), (x * cellWidth) + cellWidth, (y * cellHeight));
			}
			if (currCell.border[1]) {
				drawBorder((x * cellWidth) + cellWidth, (y * cellHeight), (x * cellWidth) + cellWidth, (y * cellHeight) + cellHeight);
			}
			if (currCell.border[2]) {
				drawBorder((x * cellWidth), (y * cellHeight) + cellHeight, (x * cellWidth) + cellWidth, (y * cellHeight) + cellHeight);
			}
			if (currCell.border[3]) {
				drawBorder((x * cellWidth), (y * cellHeight), (x * cellWidth), (y * cellHeight) + cellHeight);
			}
			// Create if statement to prevent printing if no cell contents
			if (currCell.text.length > 1) {
				drawText(x, y + 1, cellWidth, cellHeight, currCell.text, currCell.textColor);
            }
			else {
				if (y > 0 && smartMap.cellContents[y - 1][x].isBooth && (smartMap.cellContents[y - 1][x].text in xref)) {
					drawText(x, y + 1, cellWidth, cellHeight, xref[smartMap.cellContents[y - 1][x].text].orgName, currCell.textColor);
				}
			}
			x = x + 1;
		}
		y = y + 1;
	}
	drawBorder(0, canvasHeight - 2, canvasWidth, canvasHeight - 2);
	drawBorder(canvasWidth - 2, 0, canvasWidth - 2, canvasHeight);

	var link = document.createElement('a');
	var name = mapName.replace(" ", "") + ".png"
	link.download = name.valueOf();
	link.href = c.toDataURL()
	document.body.appendChild(link);
	link.click();
}

function downloadPdf() {
    var c = document.getElementById("smartMap");
    c.height = height * 60 /*3000*/ + 2;
    c.width = width * 120 /*2400*/ + 2;
    var ctx = c.getContext("2d");
    var canvasHeight = c.height;
    var canvasWidth = c.width;
    var cellWidth = /*2400/width*/120;
    var cellHeight = /*3000/height*/60;
    var y = 0;
    while (y < height) {
        var x = 0;
        while (x < width) {
            //fill rectangle first
            ctx.fillStyle = smartMap.cellContents[y][x].cellColor;
            ctx.fillRect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
            //add borders to cell
            if (smartMap.cellContents[y][x].border[0]) {
                drawBorder((x * cellWidth), (y * cellHeight), (x * cellWidth) + cellWidth, (y * cellHeight));
            }
            if (smartMap.cellContents[y][x].border[1]) {
                drawBorder((x * cellWidth) + cellWidth, (y * cellHeight), (x * cellWidth) + cellWidth, (y * cellHeight) + cellHeight);
            }
            if (smartMap.cellContents[y][x].border[2]) {
                drawBorder((x * cellWidth), (y * cellHeight) + cellHeight, (x * cellWidth) + cellWidth, (y * cellHeight) + cellHeight);
            }
            if (smartMap.cellContents[y][x].border[3]) {
                drawBorder((x * cellWidth), (y * cellHeight), (x * cellWidth), (y * cellHeight) + cellHeight);
            }
            // Create if statement to prevent printing if no cell contents
            if (smartMap.cellContents[y][x].text !== "") {
                drawText(x, y + 1, cellWidth, cellHeight, smartMap.cellContents[y][x].text, smartMap.cellContents[y][x].textColor);
            }
            else {
                if (y > 0 && smartMap.cellContents[y - 1][x].isBooth && (smartMap.cellContents[y - 1][x].text in xref)) {
                    drawText(x, y + 1, cellWidth, cellHeight, xref[smartMap.cellContents[y - 1][x].text].orgName, smartMap.cellContents[y][x].textColor);
                }
            }
            x = x + 1;
        }
        y = y + 1;
    }
    drawBorder(0, canvasHeight - 2, canvasWidth, canvasHeight - 2);
    drawBorder(canvasWidth - 2, 0, canvasWidth - 2, canvasHeight);

    var link = document.createElement('a');
    var name = mapName.replace(" ", "") + ".pdf"
    link.download = name.valueOf();
    link.href = c.toDataURL()
    document.body.appendChild(link);
    link.click();
}

function drawBorder(x, y, dx, dy) {
	var c = document.getElementById("smartMap");
	var ctx = c.getContext("2d");
	ctx.beginPath();
	ctx.moveTo(x + 1, y + 1);
	ctx.lineTo(dx + 1, dy + 1);
	ctx.stroke();
}

function drawText(x, y, cellWidth, cellHeight, str, fillColor = '#000000') {
	var c = document.getElementById("smartMap");
	var ctx = c.getContext("2d");
	x = cellWidth * (x + 0.5);
	y = cellHeight * (y - 0.5);
	ctx.font = "11px Comic Sans MS";
	ctx.fillStyle = fillColor;
	ctx.textAlign = "center";
	//handle text wrap
	var words = str.split(" ");
	var line = "";
	var lines = [];
	for (var n = 0; n < words.length; n++) {
		var testLine = line + words[n] + " ";
		var metrics = ctx.measureText(testLine);
		var testWidth = metrics.width;
		if (testWidth > (cellWidth * .85) && n > 0) {
			lines.push(line);
			line = words[n] + " ";
		} else {
			line = testLine;
		}
	}
	lines.push(line);
	var lineHeight = cellHeight * .2;
	if (lines.length > 2) {
		y -= (cellHeight * .1);
		ctx.font = "9px Comic Sans MS";
	}
	if (lines.length > 3) {
		y -= (cellHeight * .1);
	}
	if (lines.length > 4) {
		y -= (cellHeight * .1);
		ctx.font = "8px Comic Sans MS";
	}
	for (var l = 0; l < lines.length; l++) {
		ctx.fillText(lines[l], x, y);
		y += lineHeight;
	}
}