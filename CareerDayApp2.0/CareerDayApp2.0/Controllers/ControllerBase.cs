﻿/* =================================================================================================
 * File Name: ControllerBase.cs 
 * File Description: File contains the logic for getting the data used by all controllers
 * ================================================================================================= */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerDayApp2._0.Models;
using CareerDayWCFWebService;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;

namespace CareerDayApp2._0.Controllers
{
    public class ControllerBase : Controller
    {
        /// <summary>
        /// Filters out company master list based on user inputs.
        /// </summary>
        /// <param name="companies">Array of companies to search through</param>
        /// <param name="level">Level input to match in search</param>
        /// <param name="major">Major input to match in the search</param>
        /// <param name="major2">A second major to match in the search</param>
        /// <returns>An array of all companies to be displayed</returns>
        public Company[] Filter(Company[] companies, int level, int major, int major2, string venue)
        {
            List<Company> list = new List<Company>();
            HashSet<Company> total = new HashSet<Company>();
            //if no major was selected
            if (major == 0 && major2 == 0)
            {
                foreach (Company c in companies)
                {
                    if (!c.orgName.ToLower().Contains("cctest") && !c.orgName.ToLower().Contains("cccancel"))
                    {
                        total.Add(c);
                    }
                }
                //for when none major was selected but a year was selected
                if (level != 0)
                {
                    HashSet<Company> l1 = new HashSet<Company>();
                    foreach (Company c in total)
                    {
                        foreach (WantsToMeetWith m in c.WantsToMeetWithList)
                        {
                            if (m.levelIDs.Contains(level.ToString()))
                            {
                                l1.Add(c);
                                break;
                            }
                        }
                    }
                    total = l1;
                }
            }
            //if one or two majors were selected
            else
            {
                HashSet<Company> m1 = haveMajor(companies, major, level);
                HashSet<Company> m2 = haveMajor(companies, major2, level);
                //merge two sets
                foreach (Company c in m1)
                {
                    m2.Add(c);
                }
                total = m2;
            }
            //add total to list if it is the correct venue
            foreach (Company c in total)
            {
                if (venue == "" || getPrefix(c.orgBoothNo) == venue)
                {
                    if (!c.orgName.ToLower().Contains("cctest") && !c.orgName.ToLower().Contains("cccancel"))
                    {
                        list.Add(c);
                    }
                }
            }
            return list.ToArray();
        }

        /// <summary>
        /// Helper function to filter out the companies list by only one major.
        /// </summary>
        /// <param name="companies">List of companies to search through</param>
        /// <param name="major">Level input to match in search</param>
        /// <param name="level">Major input to match in search</param>
        /// <returns>A set of companies searching for the given major</returns>
        public HashSet<Company> haveMajor(Company[] companies, int major, int level)
        {
            //initialize new set to be returned
            HashSet<Company> potential = new HashSet<Company>();
            //adds all companies searching for specific major
            foreach (Company c in companies)
            {
                foreach (WantsToMeetWith m in c.WantsToMeetWithList)
                {
                    if (Convert.ToInt32(m.majorID) == major)
                    {
                        potential.Add(c);
                        break;
                    }
                }
            }
            //return return set if level was not specified, otherwise filter out result by level as well
            if (level == 0)
            {
                return potential;
            }
            else
            {
                HashSet<Company> newList = new HashSet<Company>();
                foreach (Company c in potential)
                {
                    foreach (WantsToMeetWith m in c.WantsToMeetWithList)
                    {
                        if (Convert.ToInt32(m.majorID) == major)
                        {
                            if (m.levelIDs.Contains(level.ToString()))
                            {
                                newList.Add(c);
                                break;
                            }
                            break;
                        }
                    }
                }
                return newList;
            }
        }

        /// <summary>
        /// Sorts an array of companies using Quicksort based on their booth number.
        /// </summary>
        /// <param name="companies">Array of companies to sort</param>
        /// <param name="index">Partition for Quicksort to begin with</param>
        public void boothSort(ref Company[] companies, int index)
        {
            if (companies.Length <= 0)
            {
                return;
            }
            // Remove ToString if database gives booth number as string (with prefix)

            // check if company has more than one booth number and splits it
            string compValue = companies[index].orgBoothNo;
            if (compValue.Contains('&'))
            {
                List<string> temp = compValue.Replace(" ", string.Empty).Split('&').ToList();
                compValue = temp[0];
            }
            string indexValue = formatBoothNum(compValue);
            List<Company> lower = new List<Company>();
            List<Company> upper = new List<Company>();

            // Loops through all companies other than partition value
            for (int i = 0; i < index; i++)
            {
                //int current = System.Convert.ToInt32(getBoothInt(companies[i].booth));
                compValue = companies[i].orgBoothNo;
                if (compValue.Contains('&'))
                {
                    List<string> temp = compValue.Replace(" ", string.Empty).Split('&').ToList();
                    compValue = temp[0];
                }
                // Remove ToString if database gives booth number as string (with prefix)
                string current = formatBoothNum(compValue);
                // Add to upper or lower list accordingly
                if (String.Compare(indexValue, current) > 0)
                {
                    upper.Add(companies[i]);
                }
                else
                {
                    lower.Add(companies[i]);
                }
            }

            //convert both lists to arrays
            Company[] u = upper.ToArray();
            Company[] l = lower.ToArray();

            //recursive call function with lists if has more than 1 value
            if (u.Length > 1)
            {
                boothSort(ref u, u.Length - 1);
            }
            if (l.Length > 1)
            {
                boothSort(ref l, l.Length - 1);
            }

            //replace companies array with new sorted list
            Company[] sorted = new Company[companies.Length];
            int a = 0;
            foreach (Company c in l)
            {
                sorted[a++] = c;
            }
            sorted[a++] = companies[index];
            foreach (Company c in u)
            {
                sorted[a++] = c;
            }
            companies = sorted;
            Session["notes"] = new Dictionary<string, string>();
        }

        /// <summary>
        /// Sorts an array of companies by name (alphabetical) using Quicksort.
        /// </summary>
        /// <param name="companies">Array of companies to sort</param>
        /// <param name="index">Partition to start at for Quicksort</param>
        public void nameSort(ref Company[] companies, int index)
        {
            if (companies.Length <= 0)
            {
                return;
            }
            string indexValue = companies[index].orgName;
            List<Company> lower = new List<Company>();
            List<Company> upper = new List<Company>();

            //loops through all companies other than partition value
            for (int i = 0; i < index; i++)
            {
                string current = companies[i].orgName;
                //add to upper or lower list accordingly
                if (String.Compare(indexValue, current) < 0)
                {
                    upper.Add(companies[i]);
                }
                else
                {
                    lower.Add(companies[i]);
                }
            }

            //convert both lists to arrays
            Company[] u = upper.ToArray();
            Company[] l = lower.ToArray();

            //recursive call function with lists if has more than 1 value
            if (u.Length > 1)
            {
                nameSort(ref u, u.Length - 1);
            }
            if (l.Length > 1)
            {
                nameSort(ref l, l.Length - 1);
            }

            //replace companies array with new sorted list
            Company[] sorted = new Company[companies.Length];
            int a = 0;
            foreach (Company c in l)
            {
                sorted[a++] = c;
            }
            sorted[a++] = companies[index];
            foreach (Company c in u)
            {
                sorted[a++] = c;
            }
            companies = sorted;
        }

        /// <summary>
        /// Sorts an array of companies by in diggernet first.
        /// </summary>
        /// <param name="companies">Array of companies to sort</param>
        public void recruitSort(ref Company[] companies)
        {
            List<Company> yes = new List<Company>();
            List<Company> no = new List<Company>();
            foreach (Company c in companies)
            {
                if (c.InDiggerNet)
                {
                    yes.Add(c);
                }
                else
                {
                    no.Add(c);
                }
            }
            //replaces companies with new order
            int i = 0;
            foreach (Company y in yes)
            {
                companies[i++] = y;
            }
            foreach (Company n in no)
            {
                companies[i++] = n;
            }
        }

        /// <summary>
        /// Gets all the information from the home config file and turns it into a Home object.
        /// </summary>
        /// <returns>A Home object containing info from the home config file</returns>
        public Home getHomeInfo()
        {
            Home obj = new Home();
            using (System.IO.StreamReader r = new StreamReader(Server.MapPath("~/App_Data/homeConfig.json")))
            {
                string json = r.ReadToEnd();
                obj = JsonConvert.DeserializeObject<Home>(json);
            }
            return obj;
        }

        /// <summary>
        /// Helper function for saving notes to cookies
        /// </summary>
        /// <param name="note">dictionary of notes associated with the company</string></param>
        public void SaveToNotesCookies(Dictionary<string, string> note)
        {
            HttpCookie newCookie = new HttpCookie("notes");
            foreach (var i in note)
            {
                newCookie[Server.UrlEncode(i.Key)] = Server.UrlEncode(i.Value);
            }
            newCookie.Expires = DateTime.Now.AddDays(90);
            Response.Cookies.Add(newCookie);
        }

        /// <summary>
        /// Separates the letter prefix from the rest of a booth number.
        /// </summary>
        /// <param name="boothNum">The booth number to get the prefix of</param>
        /// <returns>Prefix of the given booth number</returns>
        public string getPrefix(string boothNum)
        {
            if (boothNum.Contains("RD"))
            {
                boothNum = boothNum.Replace("RD", "");
            }
            int length = boothNum.Length;
            string returnVal = "";
            for (int i = 0; i < length; i++)
            {
                if (char.IsLetter(boothNum[i]))
                {
                    returnVal = returnVal + boothNum[i];
                }
            }

            return returnVal;
        }

        /// <summary>
        /// Formats the booth number so that the integer portion always has three digits. Used for sorting companies by booth number.
        /// </summary>
        /// <param name="boothNum">Booth number to readjust the format of</param>
        /// <returns>Reformatted booth number</returns>
        public string formatBoothNum(string boothNum)
        {
            if (boothNum != null)
            {
                int length = boothNum.Length;
                string returnVal = "";
                for (int i = 0; i < length; i++)
                {
                    if (char.IsNumber(boothNum[i]))
                    {
                        returnVal = returnVal + boothNum[i];
                    }
                }
                while (returnVal.Length < 3)
                {
                    returnVal = '0' + returnVal;
                }
                string prefix = getPrefix(boothNum);
                returnVal = prefix + returnVal;

                return returnVal;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Function used to upload a user's file into the project folder.
        /// </summary>
        /// <param name="file">File to upload</param>
        /// <param name="subFolder">Subfolder in images to save to</param>
        public void ImageUpload(HttpPostedFileBase file, string subFolder, string prevFile)
        {
            if (file != null)
            {
                if (file.ContentLength > 0)
                {
                    //remove previous file
                    string filePathRemove = Path.Combine(Server.MapPath("~/images/" + subFolder), Path.GetFileName(prevFile));
                    if (System.IO.File.Exists(filePathRemove))
                    {
                        System.IO.File.Delete(filePathRemove);
                    }
                    //add new file
                    try
                    {
                        string filePath = Path.Combine(Server.MapPath("~/images/" + subFolder), Path.GetFileName(file.FileName));
                        string FileName = file.FileName;
                        file.SaveAs(filePath);
                        ViewBag.message = "File successfully uploaded";
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = "Error" + ex.Message.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Helper function to download 30-sec intro to the client's computer
        /// </summary>
        /// <param name="fileName">name that the downloaded file is saved under</param>
        /// <param name="intro">string containing the user's 30-sec intro</param>
        public void DownloadFile(string fileName, string intro)
        {
            // zero out array used to store intro
            byte[] btFile = new byte[0];
            Array.Clear(btFile, 0, btFile.Length);
            // convert intro string to array of bytes
            btFile = System.Text.Encoding.ASCII.GetBytes(intro);

            // passes byte array to the client browser for download
            Response.Clear();
            Response.AddHeader("Content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(btFile);
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Helper function to download saved company notes to the client's computer
        /// </summary>
        /// <param name="fileName">Name that the file is saved under</param>
        /// <param name="note">Structure that stores company name and note pairs</param>
        public void DownloadFile(string fileName, Dictionary<string, string> note, Dictionary<string, string> xrefBooth)
        {
            // combine contents of 'note' into a single string
            string notes_string = "";
            foreach (string s in note.Keys)
            {
                notes_string += String.Format("{0}:\n\tBooth:\t{1}\t\n\tNotes:\t{2}\n\n", s, xrefBooth[s], note[s]);
            }

            // convert 'notes_string' into a byte array
            byte[] btFile = System.Text.Encoding.ASCII.GetBytes(notes_string);

            // passes byte array to the client browser for download
            Response.AddHeader("Content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(btFile);
            Response.End();
        }

        /// <summary>
        /// Takes csv version of the booth map and converts it to json format
        /// </summary>
        /// <param name="smartMap"></param>
        /// <param name="csvFile"></param>
        public void ParseMapCSV(string csvString, ref string jsonString)
        {
            SmartMap map = new SmartMap();

            csvString = csvString.Replace(", ,", ",,");
            // Initialize regular expressions for booth numbers
            Regex pattern = new Regex(@"^([A-Z]{1,2})(\d+)$");
            // Remove company names that use ',' alongside abbreviations/acronyms (LLC, Inc.)
            string abbreviations = @"\,([A-Za-z]{1,4})((\x22)|(\.\x22)|(\.))";
            //string abbreviations = @"\, ";
            csvString = Regex.Replace(csvString, abbreviations, " ");

            // Split the csv files into list by rows
            List<string> csvContentRows = csvString.Split('\n').ToList<string>();
            map.height = csvContentRows.Count - 1;
            map.width = csvContentRows[0].Count();
            // Split each row into a 2D array
            List<List<string>> csvContentArray = new List<List<string>>();
            foreach (string s in csvContentRows)
            {
                csvContentArray.Add(s.Split(',').ToList());
            }

            map = new SmartMap(map.height, map.width);
            CellContents[][] tempTable = new CellContents[map.height][];
            // Read in each value from 2D array into CellContents of map
            for (int i = 0; i < map.height; i++)
			{
				CellContents[] row = new CellContents[map.width];
				for (int j = 0; j < map.width; j++)
				{
					row[j] = new CellContents(csvContentArray[i][j]);

                    // Check if the current cell is a booth number
                    if (pattern.IsMatch(row[j].text))
                    {
                        tempTable[i - 1][j].CreateUpper(csvContentArray[i][j]);
                        row[j].CreateLower();
                    } else
                    {
                        row[j] = new CellContents();
                    }
                }
				
				tempTable[i] = row;
            }
            map.cellContents = tempTable;

            jsonString = JsonConvert.SerializeObject(map, Formatting.Indented);
        }

        /// <summary>
        /// Helper function that creates a new table of specified length and populates it with H
        /// </summary>
        /// <param name="height">Height of table</param>
        /// <param name="width">Width of table</param>
        /// <returns>New table of type string[][]</returns>
        public CellContents[][] makeNewTable(int height, int width)
        {
            CellContents[][] newTable = new CellContents[height][];
            for (int i = 0; i < height; i++)
            {
                CellContents[] row = new CellContents[width];
                for (int j = 0; j < width; j++)
                {
                    row[j] = new CellContents();
                }
                newTable[i] = row;
            }
            return newTable;
        }

        /// <summary>
        /// Helper function for getting all majors from database
        /// </summary>
        /// <returns>Array of strings</returns>
        public Major[] getAllMajors()
        {
            iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
            //web service call
            Major[] majors = client.GetMajorList().OrderBy(item => item.MajName).ToArray();
            client.Close();

            if (majors.Length != 0)
            {
                return majors;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets all possible degree/year levels to populate dropdown.
        /// </summary>
        /// <returns>A list of all years</returns>
        public Level[] getAllLevels()
        {
            iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
            Level[] levels = client.GetLevelList().OrderBy(item => item.LvlID).ToArray();
            client.Close();
            return levels;
        }

        /// <summary>
        /// Gets all information from the mapConfig file and turns it into Map objects.
        /// </summary>
        /// <returns>An array of Map objects</returns>
        public Map[] getAllMaps()
        {
            List<Map> mapList = new List<Map>();
            string FilePath = Server.MapPath("~/App_Data/mapConfig.json");
            using (StreamReader read = new StreamReader(FilePath))
            {
                string json = read.ReadToEnd();
                JArray jArray = JArray.Parse(json);
                foreach (JObject item in jArray)
                {
                    Map obj = JsonConvert.DeserializeObject<Map>(item.ToString());
                    mapList.Add(obj);
                }
            }
            Map[] maps = mapList.ToArray();
            return maps;
        }
        
        /// <summary>
        /// Gets a list of all JSON files in a given directory
        /// </summary>
        /// <param name="path">Path to the desired directory</param>
        /// <returns>List of JSON files in the given directory</returns>
        public FileInfo[] getAllJsons(string path)
        {
            DirectoryInfo d = new DirectoryInfo(Server.MapPath(path));
            return d.GetFiles("*.json");
        }

        /// <summary>
        /// Saves company to user's saved company list
        /// </summary>
        /// <param name="id">id of company that will be saved</param>
        /// <param name="tempNote">dictionary of saved companies and notes pair</param>
        /// <param name="companyName">name of the company to be saved</param>
        public void SaveCompany(string id, ref Dictionary<string, string> tempNote, string companyName)
        {
            iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
            Company c = client.GetCompanyById(id);
            ViewBag.company = c; //give the company to the view
            client.Close();

            //adds a company to saved list
            List<Company> companies = new List<Company>();
            //checks cookies for data and retrieve if there is
            HttpCookie retrieveSavedCookies = Request.Cookies["saved"];
            if (retrieveSavedCookies != null)
            {
                for (int i = 0; i < retrieveSavedCookies.Values.Count; i++)
                {
                    client = new iCareerDayRESTServiceClient();
                    string tempID = Server.HtmlEncode(retrieveSavedCookies.Values[i]);
                    if (tempID != "")
                    {
                        Company a = client.GetCompanyById(tempID);
                        companies.Add(a);
                    }
                    client.Close();
                }
            }

            bool isIn = false;

            //checks to see if already in saved list, if not add to list
            foreach (Company i in companies)
            {
                if (c.orgName == i.orgName)
                {
                    isIn = true;
                    break;
                }
            }
            if (!isIn)
            {
                companies.Add(c);
                //create a new notes entry to cookies for company, first checking cookies for any saved
                tempNote[c.orgName] = "";
                SaveToNotesCookies(tempNote);
            }

            //add saved list to cookie if company is not empty
            if (companies.Count > 0)
            {
                HttpCookie saves = new HttpCookie("saved");
                for (int i = 0; i < companies.Count; i++)
                {
                    saves[Server.UrlEncode(companies[i].orgName)] = companies[i].orgID.ToString();
                }
                saves.Expires = DateTime.Now.AddDays(90);
                Response.Cookies.Add(saves);
            }

            //check if saved
            if (!isIn)
            {
                foreach (Company i in companies)
                {
                    if (c.orgName == i.orgName)
                    {
                        isIn = true;
                        break;
                    }
                }
            }




        }

        /// <summary>
        /// Deletes company from user's list of saved companies
        /// </summary>
        /// <param name="name">name of the company that will be deleted</param>
        /// <param name="id">id of the company to be deleted</param>
        /// <param name="tempNote">dictionary of company and notes value pairs</param>
        /// <param name="companies">list of already saved companies</param>
        public void DeleteCompany(string name, string id, ref Dictionary<string, string> tempNote, ref List<Company> companies)
        {
            iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
            Company c = client.GetCompanyById(id);
            //ViewBag.company = c; //give the company to the view
            client.Close();
            bool isIn = false;

            //adds a company to saved list
            //companies = new List<Company>();
            //checks cookies for data and retrieve if there is
            HttpCookie retrieveSavedCookies = Request.Cookies["saved"];
            if (companies.Count == 0)
            {
                if (retrieveSavedCookies != null)
                {
                    for (int i = 0; i < retrieveSavedCookies.Values.Count; i++)
                    {
                        client = new iCareerDayRESTServiceClient();
                        string tempID = Server.HtmlEncode(retrieveSavedCookies.Values[i]);
                        if (tempID != "")
                        {
                            Company a = client.GetCompanyById(tempID);
                            companies.Add(a);
                        }
                        client.Close();
                    }
                }
            }

            //checks to see if already in saved list
            foreach (Company i in companies)
            {
                if (c.orgName == i.orgName)
                {
                    isIn = true;
                    break;
                }
            }

            if (isIn)
            {
                //remove a company from saved companies and notes cookies
                HttpCookie deleteSavedCookie = new HttpCookie("saved");
                int del = -1;
                for (int i = 0; i < companies.Count; i++)
                {
                    if (companies[i].orgName == c.orgName)
                    {
                        del = i;
                    }
                    else
                    {
                        deleteSavedCookie[Server.UrlEncode(companies[i].orgName)] = companies[i].orgID.ToString();
                    }
                }
                companies.RemoveAt(del);
                deleteSavedCookie.Expires = DateTime.Now.AddDays(90);
                Response.Cookies.Add(deleteSavedCookie);
                //remove company from notes dictionary and cookies as well
                tempNote.Remove(c.orgName);
                SaveToNotesCookies(tempNote);
            }

        }

        /// <summary>
        /// Determines whether or not a booth number indicates a resume drop
        /// </summary>
        /// <param name="boothNum">The booth number to examine</param>
        /// <returns></returns>
        public bool isResumeDrop(string boothNum)
        {
            if (boothNum.Length > 2)
            {
                if (boothNum.Substring(boothNum.Length - 2).ToLower() == "rd")
                {
                    return true;
                }
            }
            return false;
        }

		/// <summary>
		/// Used to convert number to months
		/// </summary>
		/// <returns>Dictionary of months with number strings as key and name as value</returns>
		public Dictionary<string, string> getMonths()
		{
			Dictionary<string, string> months = new Dictionary<string, string>();
			months["01"] = "January";
			months["02"] = "February";
			months["03"] = "March";
			months["04"] = "April";
			months["05"] = "May";
			months["06"] = "June";
			months["07"] = "July";
			months["08"] = "August";
			months["09"] = "September";
			months["10"] = "October";
			months["11"] = "November";
			months["12"] = "December";
			return months;
		}
	}
}