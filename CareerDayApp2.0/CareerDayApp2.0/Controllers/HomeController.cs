﻿/* =================================================================================================
 * File Name: HomeController.cs 
 * File Description: File contains the logic for getting the data and bringing up the views with the 
 *  data given 
 * ================================================================================================= */

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using CareerDayApp2._0.Models;
using CareerDayWCFWebService;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace CareerDayApp2._0.Controllers
{
    public class HomeController : ControllerBase
    {
        /// <summary>
        /// Gets homepage information and passes it into a ViewBag to build the view. 
        /// </summary>
        /// <returns> View of the home (index) page </returns>
        public ActionResult Index()
        {
            Home homeInfo = getHomeInfo();
            ViewBag.info = homeInfo;
            return View();
        }

        /// <summary>
        /// Contains all logic for creating the View of the Companies page.
        /// </summary>
        /// <param name="page">Which page number of results to display</param>
        /// <param name="number">Number of results to display for each page</param>
        /// <param name="Search">String from search bar to search for in company names</param>
        /// <param name="Major">Major to search companies for</param>
        /// <param name="Major2">A second major to search companies for</param>
        /// <param name="Level1">Level to search companies for</param>
        /// <param name="sort">Which table column to sort companies by</param>
        /// <returns>View of the companies page</returns>
        public ActionResult Companies(int page = 0, int number = 50, string Search = "", int Major = 0, int Major2 = 0, int Level1 = 0, string Venue = "", string sort = "name")
        {
            ViewBag.saved = false;
            ViewBag.delete = true;

            iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
            //Get all companies into an array
            Company[] companies;
            if (Session["companies"] == null)
            {
                companies = client.GetCompanyList();
                Session["companies"] = companies;
            }
            else
            {
                companies = (Company[])Session["companies"];
            }
            client.Close();
            companies = Filter(companies, Level1, Major, Major2, Venue);

            // check if the user has cookies stored for saved companies with notes
            Dictionary<string, string> tempNote = new Dictionary<string, string>();
            HttpCookie tempNoteCookie = Request.Cookies["notes"];
            if (tempNoteCookie != null)
            {
                foreach (string a in tempNoteCookie.Values)
                {
                    if (Server.UrlDecode(a) != null)
                    {
                        tempNote[Server.UrlDecode(a)] = Server.UrlDecode(tempNoteCookie[a]);
                    }
                }
            }

            // if user clicks save button company is added to list of saved companies
            if (Request.QueryString["Save"] == "Save")
            {
                client = new iCareerDayRESTServiceClient();
                string id = Request.QueryString["orgID"]; 
                SaveCompany(id, ref tempNote, client.GetCompanyById(id).orgName);
                client.Close(); 
            }
            // if user clicks unsave button company is removed from saved companies list
            else if (Request.QueryString["Save"] == "Unsave")
            {
                Debug.WriteLine("else");
                client = new iCareerDayRESTServiceClient();
                string id = Request.QueryString["orgID"];
                var companyList = new List<Company>();
                DeleteCompany(client.GetCompanyById(id).orgName, id, ref tempNote, ref companyList);
                client.Close();
            }

            ViewBag.notes = tempNote; 

            //sort companies by booth number, name, on campus or job posting
            ViewBag.sort = sort;
			if (sort == "booth")
			{
				boothSort(ref companies, companies.Length - 1);
			}
			else if (sort == "recruit")
			{
				recruitSort(ref companies);
			}
			else
			{
				nameSort(ref companies, companies.Length - 1);
			}

			//if search was used, get companies with that name
			if (Search != "")

            {
                // Search pattern compares beginning of each word in company name
                // with search parameter
                Regex pattern = new Regex(String.Format(@"\b{0}\S*", Search));
                // add all companies that contains those text to new temp list
                List<Company> temp = new List<Company>();
                foreach (Company company in companies)
                {
                    string name = company.orgName;
                    if (pattern.IsMatch(name))
                    {
                        temp.Add(company);
                    }
                }
                // Put companies from temp list into current companies array
                companies = temp.ToArray();
            }

            // Paging mechanism for table
            Company[] shortList;
            int startIndex = page * number;

            // elems is used to check if the last page has less elements that the specified number of elements per page
            int elems = companies.Length - startIndex;
            if (elems < number && elems >= 0)
            {
                shortList = new Company[elems];
            }
            else
            {
                shortList = new Company[number];
                elems = number;
            }

            // adds elements from master table into shortlist, starting at startingIndex
            ViewBag.major = Major;
            ViewBag.major2 = Major2;
            ViewBag.level1 = Level1;
            ViewBag.search = Search;
            int companies_length = companies.Length;
            ViewBag.count = companies_length;
            for (int i = 0; i < elems; i++)
            {
                shortList[i] = companies[startIndex + i];
            }
            ViewBag.number = number;  //set the number of elements per page
            int pages = companies.Length / number; // Set the number of pages
            if (companies.Length % number != 0)
            {
                pages++;
            }

            //create a new array of ints to use in companies view
            int[] pagesList = new int[pages];
            for (int i = 0; i < pages; i++)
            {
                pagesList[i] = i + 1;
            }

            ViewBag.pages = pagesList;
            ViewBag.page = page;
            ViewBag.companies = shortList;
            ViewBag.majors = getAllMajors();

            //add list of levels to viewbag
            ViewBag.levels = getAllLevels();

            // Get list of maps for venue dropdown 
            ViewBag.maps = getAllMaps();
            ViewBag.venue = Venue;

			//get clicked save position
			ViewBag.clicked = null;
			if (Request.QueryString["orgID"] != null)
			{
				int orgID = int.Parse(Request.QueryString["orgID"]);
				ViewBag.clicked = orgID;
			}

			//get database wipe disclaimer 
			Home homeInfo = getHomeInfo();
			string wipeDate = homeInfo.wipeDate;
			string[] splits = wipeDate.Split('-');
			ViewBag.wipeDate = getMonths()[splits[1]] + " " + splits[2];
			ViewBag.showDataWipe = homeInfo.showDataWipe;
			bool notTime = true;
			if (homeInfo.showDataWipe && DateTime.Now.CompareTo(Convert.ToDateTime(homeInfo.wipeDate)) > 0) {
				notTime = false;
				//don't need to display wipe date anymore
				ViewBag.showDataWipe = false;
			}
			ViewBag.notTime = notTime;

			return View();
        }

        /// <summary>
        /// Handles all logic for displaying a Company page.
        /// </summary>
        /// <param name="id">ID of the company whose page should be displayed</param>
        /// <param name="saved">Whether or not this company has been saved</param>
        /// <param name="delete">Becomes true when the user clicks the unsave button</param>
        /// <returns>View of the Company page</returns>
        public ActionResult Company(string id, bool saved = false, bool delete = false)
        {
            iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
            Company c = client.GetCompanyById(id);
            ViewBag.company = c; //give the company to the view
            client.Close();

            //get notes from cookies into dictionary
            Dictionary<string, string> tempNote = new Dictionary<string, string>();
            HttpCookie tempNoteCookie = Request.Cookies["notes"];
            if (tempNoteCookie != null)
            {
                foreach (string a in tempNoteCookie.Values)
                {
                    if (Server.UrlDecode(a) != null)
                    {
                        tempNote[Server.UrlDecode(a)] = Server.UrlDecode(tempNoteCookie[a]);
                    }
                }
            }

            //adds a company to saved list
            List<Company> companies = new List<Company>();
            //checks cookies for data and retrieve if there is
            HttpCookie retrieveSavedCookies = Request.Cookies["saved"];
            if (retrieveSavedCookies != null)
            {
                for (int i = 0; i < retrieveSavedCookies.Values.Count; i++)
                {
                    client = new iCareerDayRESTServiceClient();
                    string tempID = Server.HtmlEncode(retrieveSavedCookies.Values[i]);
                    if (tempID != "")
                    {
                        Company a = client.GetCompanyById(tempID);
                        companies.Add(a);
                    }
                    client.Close();
                }
            }


            //checks to see if already in saved list, if not add to list
            bool inSavedList = false;
            foreach (Company i in companies)
            {
                if (c.orgName == i.orgName)
                {
                    inSavedList = true;
                    break;
                }
            }
            
            // checks if the company has been saved by the user
            if (saved)
            {
                SaveCompany(id, ref tempNote, c.orgName);
                ViewBag.saved = true;
                ViewBag.delete = false; 
            }
            // checks if user wants to remove company from list of saved companies
            else if (delete)
            {
                companies = new List<Company>();
                DeleteCompany(c.orgName, id, ref tempNote, ref companies);
                ViewBag.saved = false;
                ViewBag.delete = true; 
            }
            else
            {
                ViewBag.saved = inSavedList;
                ViewBag.delete = false;
            }
            //get all levels/majors for company page table
            ViewBag.levels = getAllLevels();
            ViewBag.majors = getAllMajors();
            // get all majors/levels company wants to meet
            ViewBag.wantsToMeetWith = c.WantsToMeetWithList;

			//format company url
			string url = c.orgURL;
			if (url.Contains("https://"))
			{
				url = url.Replace("https://", "");
			}
			if (url.Contains("http://"))
			{
				url = url.Replace("http://", "");
			}
			if (url.Contains("www."))
			{
				url = url.Replace("www.", "");
			}
			ViewBag.url = url;

            return View();
        }

        /// <summary>
        /// Gets all maps to pass to the BoothMap view
        /// </summary>
        /// <returns>View of the BoothMap page</returns>
        public ActionResult BoothMap()
        {
            // retrieves all map objects from the mapConfig file
            ViewBag.maps = getAllMaps();
			//get database wipe disclaimer 
			Home homeInfo = getHomeInfo();
			bool notTime = true;
			if (homeInfo.showDataWipe && DateTime.Now.CompareTo(Convert.ToDateTime(homeInfo.wipeDate)) > 0)
			{
				notTime = false;
			}
			ViewBag.notTime = notTime;
			return View();
        }

        /// <summary>
        /// Contains logic for creating the View of the Tips and Tricks page.
        /// </summary>
        /// <param name="intro">The intro that the user is saving</param>
        /// <param name="saved">Tells whether or not the user is saving something. "false" for false, "Save" for true</param>
        /// <returns>View of the Tips and Tricks page</returns>
        public ActionResult TipsAndTricks(string intro = "", string saved = "false", string download = "")
        {
            // retrieve the users 30-sec intro if it is stored in the cookies
            HttpCookie introCookies = Request.Cookies["intro"];

            // retrieve information to create tips page from json file
            Dictionary<string, string> temp = new TipsTricks().allFiles;
            string FilePath = Server.MapPath("~/App_Data/tipsConfig.json");
            using (System.IO.StreamReader sr = new StreamReader(FilePath))
            {
                string json = sr.ReadToEnd();
                temp = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            }
            ViewBag.pdfs = temp;

            // store user's introduction
            string introSession = intro;
            HttpPostedFileBase file = Request.Files["userFile"];

            // saves the user's 30-sec intro in cookies in local machine
            if (saved == "Save")
            {
                // checks if user has uploaded introduction file and
                // reads from uploaded file to stores into 30-sec intro
                if (file != null && file.ContentLength != 0)
                {
                    BinaryReader br = new BinaryReader(file.InputStream);
                    byte[] binData = br.ReadBytes(file.ContentLength);

                    intro = System.Text.Encoding.UTF8.GetString(binData);
                    introSession = System.Text.Encoding.UTF8.GetString(binData);
                }
                
                HttpCookie newCookie = new HttpCookie("intro");
                newCookie.Value = Server.UrlEncode(intro);
                newCookie.Expires = DateTime.Now.AddDays(90);
                Response.Cookies.Add(newCookie);

            }
            // no file uploaded by user
            else
            {
                //checks cookies for data and retrieve if there is
                HttpCookie retrieveSavedCookies = Request.Cookies["intro"];
                if (retrieveSavedCookies != null)
                {
                    introSession = Server.UrlDecode(retrieveSavedCookies.Value);
                }
            }

            //downloads introduction to client's computer
            if (download != "" & introSession != "")
            {
                DownloadFile("Introduction.txt", introSession);
            }

            ViewBag.Info = introSession;
            return View();
        }

        /// <summary>
        /// Contains the logic required to display the (currently unused) Map page for a particular company.
        /// </summary>
        /// <param name="id">id of the company whose booth map should be displayed</param>
        /// <returns>View of the Map page</returns>
        public ActionResult Map(string id, string boothNum = "")
        {
            iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
            Company objCompany = client.GetCompanyById(id);
            client.Close();
            // Remove ToString if database gives booth number as string (with prefix)
            string prefix = getPrefix(objCompany.orgBoothNo.ToString());

            // If the booth is not associated with a specific map, go to the general BoothMap page
            if (prefix == "")
            {
                ViewBag.maps = getAllMaps();
                return View("~/Views/Home/BoothMap.cshtml");
            }

            Map theMap = new Map();
            Map[] maps = getAllMaps();
            foreach (Map m in maps)
            {
                foreach (String p in m.prefix)
                {
                    // check for prefix assigned to current map
                    if (p == prefix)
                    {
                        var virtualPath = string.Format("~/images/Maps/{0}", m.fileName);
                        var virtualSmartPath = string.Format("~/App_Data/{0}", m.smartMap);
                        theMap = m;
                        // Check to see if a SmartMap should be opened
                        if (theMap.smartMap != "" && System.IO.File.Exists(Server.MapPath(virtualSmartPath)))
                        {
                            Response.Redirect("~/Home/SmartMap?fileName=" + theMap.smartMap + "&name=" + theMap.name + "&prefix=" + theMap.prefix[0] + "&boothNum=" + boothNum);
                        }
                        // Check to see if a PDF map should be opened
                        else if (System.IO.File.Exists(Server.MapPath(virtualPath)))
                        {
                            return File(virtualPath, "application/pdf");
                        }
                        break;
                    }
                }
            }

            // Check to see if a SmartMap should be opened
            if (theMap.smartMap != "")
            {
                return SmartMap(theMap.smartMap, theMap.name, theMap.prefix[0]);
            }
            return View("~/images/" + theMap.name);
        }

        /// <summary>
        /// Contains the logic for displaying the SavedCompanies page.
        /// </summary>
        /// <param name="name">Name of the company to edit (remove or save notes)</param>
        /// <param name="notes">String of notes to be saved ("" if not applicable)</param>
        /// <param name="save">Indicates whether or not a note is being saved</param>
        /// <returns>View of the SavedCompanies page</returns>
        public ActionResult SavedCompanies(string name = "", string notes = "", string save = "false", string download = "")
        {
            List<Company> companies = new List<Company>();
            Dictionary<string, string> xrefBooth = new Dictionary<string, string>();
            //checks cookies for data to retreive user's saved companies
            HttpCookie retrieveSavedCookies = Request.Cookies["saved"];
            if (retrieveSavedCookies != null)
            {
                for (int i = 0; i < retrieveSavedCookies.Values.Count; i++)
                {
                    iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
                    // retrieve each company saved by the user
                    // add to list of saved companies and xref table of orgName to BoothNo
                    string tempID = retrieveSavedCookies.Values[i];
                    if (tempID != "")
                    {
                        try
                        {
                            Company a = client.GetCompanyById(tempID);
                            xrefBooth[a.orgName] = a.orgBoothNo;
                            companies.Add(a);
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.StackTrace);
                        }
                    }
                    client.Close();
                }
            }

            //retrieve notes from cookie
            HttpCookie noteCookies = Request.Cookies["notes"];
            Dictionary<string, string> note = new Dictionary<string, string>();
            if (noteCookies != null)
            {
                foreach (string i in noteCookies.Values)
                {
                    if (i != null)
                    {
                        note[Server.UrlDecode(i)] = Server.UrlDecode(noteCookies[i]);
                    }
                }
            }

            // If "save" has been clicked, save the notes
            if (save == "Save")
            {
                note[name] = notes;
                //store note in cookies
                SaveToNotesCookies(note);
            }
            // If "download" has been clicked, download notes to user"
            else if (download != "" & note.Count != 0)
            {
                DownloadFile("Saved_Company_Notes.txt", note, xrefBooth);
            }
            // If remove all has been clicked, clear saved companies and notes
            else if (name == "All")
            {
                //saved
                HttpCookie deleteSavedCookie = new HttpCookie("saved");
                companies.Clear();
                deleteSavedCookie.Expires = DateTime.Now.AddDays(90);
                Response.Cookies.Add(deleteSavedCookie);
                //notes
                note.Clear();
                SaveToNotesCookies(note);
            }
            //if delete has been clicked, remove company from saved and notes
            else if (name != "")
            {
                //saved
                HttpCookie deleteSavedCookie = new HttpCookie("saved");
                for (int i = 0; i < companies.Count; i++)
                {
                    if (companies[i].orgName == name)
                    {
                        DeleteCompany(name, companies[i].orgID.ToString(), ref note, ref companies);
                        break;
                    }
                }
            }
            ViewBag.notes = note;
            ViewBag.saved = companies;

			//get database wipe disclaimer 
			ViewBag.showWiped = false;
			Home homeInfo = getHomeInfo();
			string wipeDate = homeInfo.wipeDate;
			string[] splits = wipeDate.Split('-');
			ViewBag.wipeDate = getMonths()[splits[1]] + " " + splits[2];
			ViewBag.showDataWipe = homeInfo.showDataWipe;
			bool notTime = true;
			if (homeInfo.showDataWipe && DateTime.Now.CompareTo(Convert.ToDateTime(homeInfo.wipeDate)) > 0)
			{
				notTime = false;
				//saved
				HttpCookie deleteSavedCookie = new HttpCookie("saved");
				companies.Clear();
				deleteSavedCookie.Expires = DateTime.Now.AddDays(90);
				Response.Cookies.Add(deleteSavedCookie);
				//notes
				note.Clear();
				//saveToNotesCookies(note);
				//add to viewbag
				ViewBag.notes = note;
				ViewBag.saved = companies;
				//don't need to display wipe date anymore
				ViewBag.showDataWipe = false;
				ViewBag.showWiped = true;
			}
			ViewBag.notTime = notTime;

			return View();
        }

        /// <summary>
        /// Contains the logic for building the smart map page that is an interactive map.
        /// </summary>
        /// <param name="fileName">Name of map to get smart map of</param>
        /// <returns>View of the AdminMap page</returns>
        public ActionResult SmartMap(string fileName, string name, string prefix, string boothNum = "")
        {
            // Read in map info from json config
            SmartMap smartMap = new SmartMap();
            string FilePath = Path.Combine(Server.MapPath("~/App_Data/"), fileName);
            using (System.IO.StreamReader r = new StreamReader(FilePath))
            {
                string json = r.ReadToEnd();
                smartMap = JsonConvert.DeserializeObject<SmartMap>(json);
            }
            ////edit smart map to make it look better by removing empty rows
            //CellContents[][] tCells = smartMap.cellContents;
            //List<int> empRowPos = new List<int>();
            //for (int r = 0; r < tCells.Length; r++)
            //{
            //	bool empty = true;
            //	for (int c = 0; c < tCells[r].Length; c++)
            //	{
            //		if (tCells[r][c].text != "" && tCells[r][c].text != "\r")
            //		{
            //			empty = false;
            //			break;
            //		}
            //	}
            //	if (empty)
            //	{
            //		//check if lower booth row
            //		bool isLower = false;
            //		if (r > 0)
            //		{
            //			for (int c = 0; c < tCells[r-1].Length; c++)
            //			{
            //				if (tCells[r - 1][c].isBooth)
            //				{
            //					isLower = true;
            //					break;
            //				}
            //			}
            //		}
            //		if (!isLower)
            //		{
            //			empRowPos.Add(r);
            //		}
            //	}
            //}
            //tCells = new CellContents[smartMap.cellContents.Length - empRowPos.Count][];
            //int ind = 0;
            //for (int r = 0; r < smartMap.cellContents.Length; r++)
            //{
            //	if (!empRowPos.Contains(r))
            //	{
            //		tCells[ind] = smartMap.cellContents[r];
            //		ind++;
            //	}
            //}
            //smartMap.cellContents = tCells;
            //smartMap.height = tCells.Length;
			ViewBag.map = smartMap;

            // Create a dictionary of companies and booth numbers
            iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
            Company[] companies = client.GetCompanyList();
            client.Close();
            Dictionary<string, Company> boothNums = new Dictionary<string, Company>();
            List<Company> resumeDrops = new List<Company>(); 
            foreach (Company c in companies)
            {
                // Check is booth is resume drop (RD)
                if (c.orgBoothNo.Contains("RD"))
                {
                    if (getPrefix(c.orgBoothNo) == prefix)
                    {
                        resumeDrops.Add(c);
                    }
                }
                // check if dictionary already contains key for company and filter out test companies
                if (!boothNums.ContainsKey(c.orgBoothNo.ToString()))
                {
                    // check if company has more than one booth number and splits it into list
                    if (c.orgBoothNo.ToString().Contains('&'))
                    {
                        List<string> temp = c.orgBoothNo.Replace(" ", string.Empty).Split('&').ToList();
                        string preFix = getPrefix(temp[0]);
                        // store first booth number company pair into dictionary
                        boothNums.Add(temp[0], c);
                        // store subsequent booth number company pairs into dictionary
                        for (int i = 1; i < temp.Count; i++)
                        {
                            temp[i] = preFix + temp[i];
                            boothNums.Add(temp[i], c);
                        }
                    }
                    // add companies with only one booth number into dictionary
                    else
                    {
                        boothNums.Add(c.orgBoothNo.ToString(), c);
                    }
                }
            }
            ViewBag.boothNums = boothNums;

            //checks cookies for data and retrieve if there is
            List<string> saved = new List<string>();
			HttpCookie retrieveSavedCookies = Request.Cookies["saved"];
			if (retrieveSavedCookies != null)
            {
				client = new iCareerDayRESTServiceClient();
				for (int i = 0; i < retrieveSavedCookies.Values.Count; i++)
                {
                    string tempID = retrieveSavedCookies.Values[i];
                    if (tempID != "")
                    {
						Company tempC = client.GetCompanyById(tempID);
                        saved.Add(tempC.orgBoothNo);
						
                    }
                }
				client.Close();
			}


            ViewBag.isRD = isResumeDrop(boothNum);
            ViewBag.saved = saved;
            ViewBag.resumeDrops = resumeDrops;
            ViewBag.mapName = name;
			ViewBag.boothNum = boothNum;
            return View();
        }

        /// <summary>
        /// Contains logic for building the cookies page.
        /// </summary>
        /// <returns>View of the Cookies page</returns>
        public ActionResult Cookies()
        {
            //ViewData["Title"] = "Cookies";
            return View();
        }

        /// <summary>
        /// Contains logic for building the view of the Resume Drop page
        /// </summary>
        /// <returns>The view of the Resume Drop page</returns>
        public ActionResult ResumeDrop()
        {
            iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
            Company[] companies = client.GetCompanyList();
            client.Close();
            // stores resume drop companies in company/prefix key/value pair
            Dictionary<Company, String> resumeDrops = new Dictionary<Company, string>(); 
            foreach (Company c in companies)
            {
                if (c.orgBoothNo.Contains("RD"))
                {
                    resumeDrops.Add(c, getPrefix(c.orgBoothNo));
                }
            }

            ViewBag.resumeDrops = resumeDrops;
            ViewBag.maps = getAllMaps(); 
            return View(); 
        }

        /// <summary>
        /// Unused function used to display an error page.
        /// </summary>
        /// <returns>null</returns>
        public ActionResult Error()
        {
            return null;
        }
    }
}
