﻿/* =================================================================================================
 * File Name: AdminController.cs 
 * File Description: File contains the logic for getting the data and bringing up the views pertaining
 * to the Admin functionality
 * ================================================================================================= */

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerDayApp2._0.Models;
using CareerDayWCFWebService;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;

namespace CareerDayApp2._0.Controllers
{
    [Authorize(Users = @"ADIT\bsiebuhr, ADIT\lcain, ADIT\byjones, ADIT\emanzer, ADIT\joannaturner")]
    public class AdminController : ControllerBase
    {
        /// <summary>
        /// Contains the logic for building the main admin page.
        /// </summary>
        /// <returns>View of the Admin page</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Contains the logic of building the admin page that controls Home information.
        /// </summary>
        /// <param name="semester">String of new semester</param>
        /// <param name="date">String of new date</param>
        /// <param name="timeNetwork">String of new time for networking event</param>
        /// <param name="timeCampus">String of new time for general career day</param>
        /// <param name="events">Name of new events pdf to upload</param>
        /// <param name="saveAll">Indicates whether the user has clicked the save all button</param>
        /// <returns>View of the AdminHome page</returns>
        public ActionResult AdminHome(string semester = "", string date = "", string timeNetwork = "", string timeCampus = "", string events = "", string saveAll = "false", bool showDataWipe = false, string wipeDate = "", string saveImage = "", int theIndex = -1)
        {
            string subFolder = "Home";
            string filePath = Server.MapPath("~/App_Data/homeConfig.json");
            Home tempHomeObject = getHomeInfo();

            if (saveImage == "addImage")
            {
                tempHomeObject.AddImage();
            }
            else if (theIndex >= 0)
            {
                string filePathRemove = Path.Combine(Server.MapPath("~/images/Home/"), Path.GetFileName(tempHomeObject.GetCarousel()[theIndex].image));
                if (System.IO.File.Exists(filePathRemove))
                {
                    System.IO.File.Delete(filePathRemove);
                }
                tempHomeObject.RemoveImage(theIndex);
            }

            if (saveAll == "Save All")
            {
                // Save all information
                tempHomeObject.semester = semester;
                tempHomeObject.date = date;
                tempHomeObject.timeNetwork = timeNetwork;
                tempHomeObject.timeCampus = timeCampus;
                tempHomeObject.showDataWipe = showDataWipe;
                tempHomeObject.wipeDate = wipeDate;
                HttpPostedFileBase file = Request.Files["events"];
                // Does the 
                if (events != "")
                {
                    ImageUpload(file, subFolder, tempHomeObject.Events);
                    tempHomeObject.Events = file.FileName;
                }
                for (int i = 0; i < tempHomeObject.GetCarouselSize(); ++i)
                {
                    tempHomeObject.GetCarousel()[i].caption = Request.Form["caption" + i];
                    file = Request.Files["photo" + i];
                    if (file != null && file.FileName != "")
                    {
                        ImageUpload(file, subFolder, tempHomeObject.GetCarousel()[i].image);
                        tempHomeObject.GetCarousel()[i].image = file.FileName;
                    }
                    tempHomeObject.GetCarousel()[i].altText = Request.Form["altText" + i];
                }

                // Write the information to the json
                string json = JsonConvert.SerializeObject(tempHomeObject, Formatting.Indented);
                using (System.IO.StreamWriter r = new StreamWriter(filePath))
                {
                    r.Write(json);
                }
            }

            // Pass info to the view
            ViewBag.info = tempHomeObject;
            return View();

        }

        /// <summary>
		/// Contains the logic for building the admin page that controls maps.
		/// </summary>
		/// <param name="newMap">Name of new map pdf to upload</param>
		/// <returns>View of the AdminMap page</returns>
        public ActionResult AdminMap(string newMap = "")
        {
            List<Map> maps = getAllMaps().ToList<Map>();
            string FilePath = Server.MapPath("~/App_Data/mapConfig.json");

            if (newMap == "Add New Map")
            {
                maps.Add(new Map("New Map"));
                string json = JsonConvert.SerializeObject(maps, Formatting.Indented);
                using (System.IO.StreamWriter r = new StreamWriter(FilePath))
                {
                    r.Write(json);
                }
            }
            ViewBag.maps = maps.ToArray<Map>();
            ViewBag.files = getAllJsons("~/App_Data");
            return View();
        }

        /// <summary>
        /// Second function to build an admin page that will save user input.
        /// </summary>
        /// <param name="names">Name of the map to save</param>
        /// <param name="smartMaps">SmartMap file to save</param>
        /// <param name="prefixes">Prefix of the map to save</param>
        /// <param name="rdPrefixes">Resume drop prefix of the map to save</param>
        /// <param name="theIndex">Index of the map to save in the list of maps</param>
        /// <param name="remove">Tells whether or not the map should be remove (true if remove = "Remove Map")</param>
        /// <returns>View of the AdminMap page</returns>
        [HttpPost]
        public ActionResult AdminMap(int theIndex = 0, string remove = "")
        {
            Map[] allMaps = getAllMaps();
            string subFolder = "Maps";

            // If remove maps, remove the map
            if (remove == "Remove Map")
            {
                /**** TODO extract this functionality to separate function for deletion ****/
                // Remove the associated smart map file
                string filePathRemove = Path.Combine(Server.MapPath("~/App_Data/"), Path.GetFileName(allMaps[theIndex].smartMap));
                if (System.IO.File.Exists(filePathRemove))
                {
                    System.IO.File.Delete(filePathRemove);
                }

                // Remove the associated pdf map file
                filePathRemove = Path.Combine(Server.MapPath("~/images/" + subFolder), Path.GetFileName(allMaps[theIndex].fileName));
                if (System.IO.File.Exists(filePathRemove))
                {
                    System.IO.File.Delete(filePathRemove);
                }

                List<Map> allMapsList = allMaps.ToList<Map>();
                allMapsList.RemoveAt(theIndex);
                allMaps = allMapsList.ToArray<Map>();
            }

            // Convert new maps to json format
            string json_string = JsonConvert.SerializeObject(allMaps, Formatting.Indented);
            string FilePath = Server.MapPath("~/App_Data/mapConfig.json");
            using (StreamWriter r = new StreamWriter(FilePath))
            {
                r.Write(json_string);
            }

            // Pass data to the view
            ViewData["Title"] = "AdminMap";
            ViewBag.maps = allMaps;
            //ViewBag.files = getAllJsons("~/App_Data");

            return View();

        }

        /// <summary>
        /// Contains the logic for building the admin page that controls the selected map
        /// </summary>
        /// <param name="mapConfigIndex">Index of the currently selected map</param>
        /// <returns></returns>
        public ActionResult SelectedMap(string names = "", string smartMaps = "", string prefixes = "", string rdPrefixes = "", int mapConfigIndex = 0, string saveMap = "false", string remove = "", string csv = "")
        {
            Debug.WriteLine(mapConfigIndex);
            // Retrieve all maps objects from the mapConfig file
            // Retrieve selected map based on index
            Map[] allMaps = getAllMaps();
            Map selectedMap = allMaps[mapConfigIndex];
            string subFolder = "Maps";

            if (remove == "Remove Smart Map")
            {
                // Remove the smart map file
                // and set smart map name to empty
                string filePathRemove = Path.Combine(Server.MapPath("~/App_Data/"), Path.GetFileName(selectedMap.smartMap));
                if (System.IO.File.Exists(filePathRemove))
                {
                    System.IO.File.Delete(filePathRemove);
                }
                allMaps[mapConfigIndex].smartMap = "";
            }
            else if (csv != "")
            {
                // Read in the csv file as a string
                HttpPostedFileBase csvFile = Request.Files["csvResponse"];
                BinaryReader br = new BinaryReader(csvFile.InputStream);
                byte[] binData = br.ReadBytes(csvFile.ContentLength);
                string csvString = System.Text.Encoding.UTF8.GetString(binData);
                string jsonString = "";

                /**** TODO ParseMapCSV currently throws out of bounds exception ****/

                //ParseMapCSV(csvString, ref jsonString);
                string mapName = selectedMap.name + ".json";
                string smartMapPath = Path.Combine(Server.MapPath("~/App_Data/"), mapName);
                using (StreamWriter sw = new StreamWriter(smartMapPath))
                {
                    sw.Write(jsonString);
                }
            }
            else if (saveMap == "Save Map")
            {
                // Check that save button was pressed
                // Save new information into the map object
                // Replace existing map object at current index
                selectedMap.name = names;
                selectedMap.smartMap = smartMaps;
                selectedMap.prefix = new string[] { prefixes, rdPrefixes };
                HttpPostedFileBase file = Request.Files["images"];
                if (file.FileName != "")
                {
                    ImageUpload(file, subFolder, selectedMap.fileName);
                    selectedMap.fileName = file.FileName;
                }

                if (Request.Form["mapType"] == "true")
                {
                    selectedMap.isBoothMap = true;
                }
                else
                {
                    selectedMap.isBoothMap = false;

                }

                allMaps[mapConfigIndex] = selectedMap;
            }
            // Write to map config file
            string json = JsonConvert.SerializeObject(allMaps, Formatting.Indented);
            string filePath = Server.MapPath("~/App_Data/mapConfig.json");
            using (System.IO.StreamWriter sw = new StreamWriter(filePath))
            {
                sw.Write(json);
            }


            ViewBag.selectedMap = selectedMap;
            ViewBag.mapConfigIndex = mapConfigIndex;
            ViewBag.files = getAllJsons("~/App_Data");

            return View();
        }

        /// <summary>
        /// Contains logic for building an admin page that will allow the user to edit/create a smart map
        /// </summary>
        /// <param name="fileName">Name of smartmap to edit</param>
        /// <param name="change">Type of change to make on smartmap (saveAll, dimensions)</param>
        /// <param name="width">Width to change smart map to</param>
        /// <param name="height">Height to change smart map to</param>
        /// <returns> View of EditMap page </returns>
        public ActionResult EditMap(string fileName = "", int width = 0, int height = 0, string change = "", string jsonString = "", int index = 0)
        {

            ViewBag.index = index; 
            ViewData["Title"] = "Edit Map";
            ViewBag.filename = fileName;

            // Read in map info from json config
            SmartMap map = new SmartMap();
            if (fileName != "")
            {
                if (!fileName.Contains(".json"))
                {
                    fileName += ".json";
                }
                string FilePath = Server.MapPath("~/App_Data/" + fileName);
                if (!System.IO.File.Exists(FilePath))
                {
                    string save = JsonConvert.SerializeObject(map, Formatting.Indented);
                    using (StreamWriter r = new StreamWriter(FilePath))
                    {
                        r.Write(save);
                        r.Close();
                    }

                }
                using (System.IO.StreamReader r = new StreamReader(Server.MapPath("~/App_Data/" + fileName)))
                {
                    string json = r.ReadToEnd();
                    map = JsonConvert.DeserializeObject<SmartMap>(json);
                    r.Close();
                }
            }
            ViewBag.mapName = fileName.Replace(".json", "");

            //changes to dimension
            if (change == "dimensions")
            {
                //if new dimensions are smaller or equal to old dimensions
                if (height < map.height && width < map.width)
                {
                    CellContents[][] newTable = new CellContents[height][];
                    for (int i = 0; i < height; i++)
                    {
                        CellContents[] row = new CellContents[width];
                        for (int j = 0; j < width; j++)
                        {
                            row[j] = map.cellContents[i][j];
                        }
                        newTable[i] = row;
                    }
                    map.cellContents = newTable;
                }
                //height
                if (height > map.height || width > map.width)
                {
                    CellContents[][] newTable = makeNewTable(height, width);
                    for (int i = 0; i < map.cellContents.Length; i++)
                    {
                        for (int j = 0; j < map.cellContents[i].Length; j++)
                        {
                            newTable[i][j] = map.cellContents[i][j];
                        }
                    }
                    map.cellContents = newTable;
                }
                //saves only when dimensions are different
                if (map.height != height || map.width != width)
                {
                    map.height = height;
                    map.width = width;
                }
            }
            //save all
            else if (change == "saveAll")
            {
                int mapHeight = int.Parse(Request.Form["mapHeight"]);
                int mapWidth = int.Parse(Request.Form["mapWidth"]);
                CellContents[][] newTable = new CellContents[mapHeight][];
                for (int i = 0; i < mapHeight; i++)
                {
                    CellContents[] row = new CellContents[mapWidth];
                    for (int j = 0; j < mapWidth; j++)
                    {
                        row[j] = new CellContents();
                        row[j].text = Request.Form["text" + i + ":" + j];
                        row[j].textColor = Request.Form["textColor" + i + ":" + j];
                        row[j].cellColor = Request.Form["cellColor" + i + ":" + j];
                        row[j].isBooth = bool.Parse(Request.Form["isBooth" + i + ":" + j]);
                        bool[] borders = new bool[4];
                        borders[0] = bool.Parse(Request.Form["topBorder" + i + ":" + j]);
                        borders[1] = bool.Parse(Request.Form["rightBorder" + i + ":" + j]);
                        borders[2] = bool.Parse(Request.Form["bottomBorder" + i + ":" + j]);
                        borders[3] = bool.Parse(Request.Form["leftBorder" + i + ":" + j]);
                        row[j].border = borders;
                    }
                    newTable[i] = row;
                }
                map.cellContents = newTable;
                map.height = mapHeight;
                map.width = mapWidth;
            }

            //save map to json file of map
            if (change != "")
            {
                string save = JsonConvert.SerializeObject(map, Formatting.Indented);
                using (System.IO.StreamWriter r = new StreamWriter(Server.MapPath("~/App_Data/" + fileName)))
                {
                    r.Write(save);
                    r.Close();
                }
            }

            // Create a dictionary of companies and booth numbers
            iCareerDayRESTServiceClient client = new iCareerDayRESTServiceClient();
            Company[] companies = client.GetCompanyList();
            client.Close();
            Dictionary<string, Company> xrefBoothCompany = new Dictionary<string, Company>();
            foreach (Company c in companies)
            {
                // check if dictionary already contains key for company and filter out test companies
                if (!xrefBoothCompany.ContainsKey(c.orgBoothNo.ToString()))
                {
                    // check if company has more than one booth number and splits it into list
                    if (c.orgBoothNo.ToString().Contains('&'))
                    {
                        List<string> temp = c.orgBoothNo.Replace(" ", string.Empty).Split('&').ToList();
                        string preFix = getPrefix(temp[0]);
                        // store first booth number company pair into dictionary
                        xrefBoothCompany.Add(temp[0], c);
                        // store subsequent booth number company pairs into dictionary
                        for (int i = 1; i < temp.Count; i++)
                        {
                            temp[i] = preFix + temp[i];
                            xrefBoothCompany.Add(temp[i], c);
                        }
                    }
                    // add companies with only one booth number into dictionary
                    else
                    {
                        xrefBoothCompany.Add(c.orgBoothNo.ToString(), c);
                    }
                }
            }
            ViewBag.xrefBoothCompany = xrefBoothCompany;
            ViewBag.map = map;
            return View();
        }

        /// <summary>
        /// Contains the logic for building the admin page that controls the pdfs on the tips and tricks page
        /// </summary>
        /// <param name="tips">Name of tips and tricks pdf</param>
        /// <param name="dress">Name of dress to impress pdf</param>
        /// <param name="networking">Name of networking pdf</param>
        /// <param name="intro">Name of 30 sec intro pdf</param>
        /// <param name="saveAll">Tells whether user has clicked the all saved button</param>
        /// <returns>View of the AdminTips page</returns>
        public ActionResult AdminTips(string tips = "", string dress = "", string networking = "", string intro = "", string saveAll = "")
        {
            string subFolder = "Tips";
            string FilePath = Server.MapPath("~/App_Data/tipsConfig.json");

            if (saveAll != "")
            {
                Dictionary<string, string> temp = new TipsTricks().allFiles;
                using (System.IO.StreamReader sr = new StreamReader(FilePath))
                {
                    string json = sr.ReadToEnd();
                    temp = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                }
                HttpPostedFileBase file = Request.Files["tips"];
                if (tips != "")
                {
                    ImageUpload(file, subFolder, temp["tips"]);
                    temp["tips"] = file.FileName;
                }
                file = Request.Files["dress"];
                if (dress != "")
                {
                    ImageUpload(file, subFolder, temp["dress"]);
                    temp["dress"] = file.FileName;
                }
                file = Request.Files["networking"];
                if (networking != "")
                {
                    ImageUpload(file, subFolder, temp["networking"]);
                    temp["networking"] = file.FileName;

                }
                file = Request.Files["intro"];
                if (intro != "")
                {
                    ImageUpload(file, subFolder, temp["intro"]);
                    temp["intro"] = file.FileName;

                }
                string save = JsonConvert.SerializeObject(temp, Formatting.Indented);
                using (System.IO.StreamWriter r = new StreamWriter(FilePath))
                {
                    r.Write(save);
                    r.Close();
                }
            }

            // Get current tips & tricks to pass to view
            Dictionary<string, string> temp2 = new TipsTricks().allFiles;
            using (System.IO.StreamReader sr = new StreamReader(FilePath))
            {
                string json = sr.ReadToEnd();
                temp2 = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            }
            ViewBag.pdfs = temp2;

            return View();
        }

    }
}